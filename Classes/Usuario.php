<?php
	require_once("conexao.php");

	class Usuario{
		function validaLogin($ds_usuario, $ds_senha)  
		{
			try{
				$con = Conecta::criarConexao();
				//*$ds_senha = hash("SHA512", $ds_senha);
				
				$select = "SELECT 
							id_usuario, ds_usuario, id_perfil, id_empresa
						FROM tb_usuario  
						WHERE ds_usuario = :ds_usuario and ds_senha = :ds_senha";

				$stmt = $con->prepare($select);
			   	$params = array(':ds_usuario' => $ds_usuario,
			   					':ds_senha' => $ds_senha);
			   
			    $stmt->execute($params); 
 
			    return  $stmt->fetch(); 
				
			}	
			catch(Exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();	
			} 
		}
		function buscarUsuario($id_usuario)  
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT *
						FROM tb_usuario u
						inner join tb_empresa e ON u.id_empresa = e.id_empresa
						WHERE id_usuario = :id_usuario";

				$stmt = $con->prepare($select);
			   	$params = array(':id_usuario' => $id_usuario);
			   
			    $stmt->execute($params); 
 
			    return  $stmt->fetch(); 
				
			}	
			catch(Exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();	
			} 
		}
		public function listarOptionsPerfil($st_tipo_perfil, $id_usuario, $id = null) 
		{
			try{
				$con = Conecta::criarConexao();
				if($st_tipo_perfil == 1){
					$select = "SELECT * FROM tb_cliente WHERE id_usuario = :id_usuario";
					$stmt = $con->prepare($select);
					$params = array(':id_usuario' => $id_usuario);
					$stmt->execute($params);

					$options = "";
					$id_cliente = $id;
					while($dados = $stmt->fetch())
					{
						if($id_cliente == $dados['id_cliente'])
						{
							$options.= "<option value='".$dados['id_cliente']."' selected>".$dados['ds_nome']."</option>";
						}
						else
						{
							$options.= "<option value='".$dados['id_cliente']."'>".$dados['ds_nome']."</option>";
						}
						

						
					}
				}
				if($st_tipo_perfil == 2){
					$select = "SELECT * FROM tb_funcionario WHERE id_usuario = :id_usuario";
					$stmt = $con->prepare($select);
					$params = array(':id_usuario' => $id_usuario);
					$stmt->execute($params);

					$options = "";
					$id_funcionario = $id;
					while($dados = $stmt->fetch())
					{
						if($id_funcionario == $dados['id_funcionario'])
						{
							$options.= "<option value='".$dados['id_funcionario']."' selected>".$dados['ds_nome']."</option>";
						}
						else
						{
							$options.= "<option value='".$dados['id_funcionario']."'>".$dados['ds_nome']."</option>";
						}
						

						
					}
				}
				if($st_tipo_perfil == 3){
					$select = "SELECT * FROM tb_ponto WHERE id_usuario = :id_usuario";
					$stmt = $con->prepare($select);
					$params = array(':id_usuario' => $id_usuario);
					$stmt->execute($params);

					$options = "";
					$id_ponto = $id;
					while($dados = $stmt->fetch())
					{
						if($id_ponto == $dados['id_ponto'])
						{
							$options.= "<option value='".$dados['id_ponto']."' selected>".$dados['ds_localidade']."</option>";
						}
						else
						{
							$options.= "<option value='".$dados['id_ponto']."'>".$dados['ds_localidade']."</option>";
						}
						

						
					}
				}
				
				
				return $options;

			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
				print $e->getMessage();
			}

			
		}


	}

?>