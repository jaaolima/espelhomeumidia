<?php
    require_once("conexao.php");
    class Documento{
        public function gravarDocumento()
		{
			$id_usuario	    = $_REQUEST['id_usuario'];
			$ds_nome	    = $_REQUEST['ds_nome'];
			$ds_documento	= $_FILES['ds_documento'];
			$st_tipo_perfil	= $_REQUEST['st_tipo_perfil'];
			$id_perfil	    = $_REQUEST['id_perfil'];


			$nome = $ds_documento['name'];
			$tipo = $ds_documento['type'];
			$tmp = $ds_documento['tmp_name'];
			$tamanho = $ds_documento['size'];

			//gravar foto
			$tamanho = 20000000;

			$error = array();
			$tamanho_mb = $tamanho/1024/1024;

			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg|doc|docx|pdf){1}$/i", $nome, $ext);
			// Gera um nome único para o arquivo
			$nome_arquivo = md5(uniqid(time())) . "arquivo.". $ext[1];
			// Caminho de onde ficará o arquivo
			$caminho_arquivo = "/home/u981186774/domains/espelhomeumidia.com/public_html/docs_documentos/" . $nome_arquivo;

			$gravar_caminho_arquivo = "docs_documentos/" . $nome_arquivo;
			

			// Faz o upload da imagem para seu respectivo caminho
			$moved = move_uploaded_file($tmp,  $caminho_arquivo);

			
			try{
				$con = Conecta::criarConexao();
				$insert = "INSERT into tb_documento (id_usuario, ds_nome, ds_documento, st_tipo_perfil, id_perfil)
							VALUES (:id_usuario, :ds_nome, :ds_documento, :st_tipo_perfil, :id_perfil)";
				
				$stmt = $con->prepare($insert);
				
				$params = array(':id_usuario' => $id_usuario,
								':ds_nome' => $ds_nome,
								':ds_documento' => $gravar_caminho_arquivo,
								':st_tipo_perfil' => $st_tipo_perfil,
								':id_perfil' => $id_perfil);
                                
				$stmt->execute($params);

				
				
				echo "Dados gravados com sucesso!"; 
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			} 
        }
        public function listarDocumento($id_usuario) 
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT d.*, c.ds_nome as ds_cliente, po.ds_localidade, f.ds_nome as ds_funcionario
							FROM tb_documento d
							left join tb_cliente c on d.id_perfil = c.id_cliente
							left join tb_ponto po on d.id_perfil = po.id_ponto
							left join tb_funcionario f on d.id_perfil = f.id_funcionario
							where d.id_usuario = :id_usuario";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_usuario' => $id_usuario);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function listarDocumentoPonto($id_ponto) 
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT *
							FROM tb_documento d
							where d.id_perfil = :id_ponto
							and st_tipo_perfil =3";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_ponto' => $id_ponto);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}

		public function listarDocumentoCliente($id_cliente) 
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT *
							FROM tb_documento d
							where d.id_perfil = :id_cliente
							and st_tipo_perfil =1";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_cliente' => $id_cliente);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}

		public function listarDocumentoFuncionario($id_cliente) 
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT *
							FROM tb_documento d
							where d.id_perfil = :id_cliente
							and st_tipo_perfil =2";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_cliente' => $id_cliente);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function BuscarDadosDocumento($id_documento)
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT id_documento, ds_nome, nu_telefone
							FROM tb_documento 
							where id_documento = :id_documento";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_documento' => $id_documento);
				
				
				$stmt->execute($params);

				return $stmt->fetch();
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function gravarAlterarDocumento(array $dados)
		{
			
			$ds_nome		= $dados['ds_nome'];
			$nu_telefone    = $dados['nu_telefone'];
            $id_documento    = $dados['id_documento'];
			
			
			try{
				$con = Conecta::criarConexao();
				$update = "UPDATE tb_documento set ds_nome = :ds_nome, nu_telefone = :nu_telefone
						WHERE id_documento = :id_documento";
				
				$stmt = $con->prepare($update);
				
				$params = array(':ds_nome' => $ds_nome, 
								':nu_telefone' => $nu_telefone,
                                ':id_documento' => $id_documento);
				$stmt->execute($params);

				
				echo "Dados alterados com sucesso!";
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function excluirDocumento(array $dados)  
		{
			$id_documento = $dados["id_documento"];
			try{
				$con = Conecta::criarConexao();
				
				$select = "delete from tb_documento
							where id_documento=:id_documento";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_documento' => $id_documento);
				
				$stmt->execute($params);
				
					 
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
        
    }



?>