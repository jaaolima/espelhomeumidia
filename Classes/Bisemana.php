<?php
    require_once("conexao.php");
    class Bisemana{
        public function listarBisemana($id_ponto)
		{
			try{
				$con = Conecta::criarConexao();
				
				/*$selectBisemana = "SELECT id_bisemana from tb_alugado where id_ponto=:id_ponto";
				$stmtBisemana = $con->prepare($selectBisemana); 
				$paramsBisemana = array(':id_ponto' => $id_ponto);

				$stmtBisemana->execute($paramsBisemana);

				/*$array = explode(',', $stmtBisemana); 
 
				echo $array;*/
				
				$select = "SELECT id_bisemana, ds_bisemana, dt_final, dt_inicial
							FROM tb_bisemana
							where dt_final > curdate()
							and dt_inicial not in (select dt_inicial from tb_alugado where id_ponto=:id_ponto)
							and dt_final not in (select dt_final from tb_alugado where id_ponto=:id_ponto)
							order by dt_inicial asc";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_ponto' => $id_ponto);
				
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function listarBisemanaTodas()
		{
			try{
				$con = Conecta::criarConexao();
				
				/*$selectBisemana = "SELECT id_bisemana from tb_alugado where id_ponto=:id_ponto";
				$stmtBisemana = $con->prepare($selectBisemana); 
				$paramsBisemana = array(':id_ponto' => $id_ponto);

				$stmtBisemana->execute($paramsBisemana);

				/*$array = explode(',', $stmtBisemana); 

				echo $array;*/
				
				$select = "SELECT id_bisemana, ds_bisemana, dt_final, dt_inicial
							FROM tb_bisemana
							where dt_final > curdate()";
				
				$stmt = $con->prepare($select); 
				
				$stmt->execute();

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
        
    }



?>