<?php
    require_once("conexao.php");
    class Cliente{
        public function gravarCliente()
		{
			$id_usuario	    = $_POST['id_usuario'];
			$id_empresa	    = $_POST['id_empresa'];
			$ds_nome	    = $_POST['ds_nome'];
			$ds_empresa	    = $_POST['ds_empresa'];
			$nu_telefone	= $_POST['nu_telefone'];
			$nu_cnpj		= $_POST['nu_cnpj'];
			$ds_email		= $_POST['ds_email'];

			
			try{
				$con = Conecta::criarConexao();
				$insert = "INSERT into tb_cliente (id_usuario, id_empresa, ds_nome, ds_empresa, nu_telefone,nu_cnpj, ds_email)
							VALUES (:id_usuario, :id_empresa, :ds_nome, :ds_empresa, :nu_telefone, :nu_cnpj, :ds_email)";
				
				$stmt = $con->prepare($insert);
				
				$params = array(':id_usuario' => $id_usuario,
								':id_empresa' => $id_empresa,
								':ds_nome' => $ds_nome,
								':ds_empresa' => $ds_empresa,
								':nu_telefone' => $nu_telefone,
								':nu_cnpj' => $nu_cnpj,
								':ds_email' => $ds_email);
                                
				$stmt->execute($params);
				
				echo "Dados gravados com sucesso!"; 
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			} 
        }
		public function listarOptionsEmpresa($id_usuario)
		{
			try{
				$con = Conecta::criarConexao();

				
				$select = "SELECT id_cliente, ds_empresa
							FROM tb_cliente
							where id_usuario = :id_usuario"; 
				
				$stmt = $con->prepare($select);
				$params = array(':id_usuario' => $id_usuario);
				$stmt->execute($params);

				$options = "<option value=''>Selecione..</option>";

				while($dados = $stmt->fetch())
				{
					$options.= "<option value='".$dados['id_cliente']."'>".$dados['ds_empresa']."</option>";
				}
				return $options;

			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print $e->getMessage();
			}
		}
        public function listarCliente($id_empresa)
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT id_cliente, ds_nome, ds_empresa, nu_telefone, ds_email
							FROM tb_cliente
							where id_empresa = :id_empresa";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_empresa' => $id_empresa);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function BuscarDadosCliente($id_cliente)
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT id_cliente, ds_nome, ds_empresa, nu_telefone, ds_email, nu_cnpj, nu_cep, ds_complemento, nu_cep, ds_endereco
							FROM tb_cliente 
							where id_cliente = :id_cliente";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_cliente' => $id_cliente);
				
				
				$stmt->execute($params);

				return $stmt->fetch();
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function gravarAlterarCliente(array $dados)
		{
			
			$ds_nome		= $dados['ds_nome'];
			$ds_empresa    	= $dados['ds_empresa'];
			$nu_telefone    = $dados['nu_telefone'];
			$nu_cnpj    	= $dados['nu_cnpj'];
			$ds_email    	= $dados['ds_email'];
			$id_cliente    	= $dados['id_cliente'];
			
			
			try{
				$con = Conecta::criarConexao();
				$update = "UPDATE tb_cliente set ds_nome = :ds_nome, ds_empresa = :ds_empresa, nu_telefone = :nu_telefone, nu_cnpj = :nu_cnpj, ds_email = :ds_email
						WHERE id_cliente = :id_cliente";
				
				$stmt = $con->prepare($update);
				
				$params = array(':ds_nome' => $ds_nome, 
								':ds_empresa' => $ds_empresa,
								':nu_telefone' => $nu_telefone,
								':nu_cnpj' => $nu_cnpj,
								':ds_email' => $ds_email,
								':id_cliente' => $id_cliente);
				$stmt->execute($params);

				
				echo "Dados alterados com sucesso!";
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function excluirCliente(array $dados) 
		{
			$id_cliente = $dados["id_cliente"];
			try{
				$con = Conecta::criarConexao();
				
				$select = "delete from tb_cliente
							where id_cliente=:id_cliente";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_cliente' => $id_cliente);
				
				$stmt->execute($params);
				
					 
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
        
    }



?>