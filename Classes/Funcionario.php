<?php
    require_once("conexao.php");
    class Funcionario{
        public function gravarFuncionario()
		{
			$id_usuario	    = $_POST['id_usuario'];
			$ds_nome	    = $_POST['ds_nome'];
			$nu_telefone	    = $_POST['nu_telefone'];

			
			try{
				$con = Conecta::criarConexao();
				$insert = "INSERT into tb_funcionario (id_usuario, ds_nome, nu_telefone)
							VALUES (:id_usuario, :ds_nome, :nu_telefone)";
				
				$stmt = $con->prepare($insert);
				
				$params = array(':id_usuario' => $id_usuario,
								':ds_nome' => $ds_nome,
								':nu_telefone' => $nu_telefone);
                                
				$stmt->execute($params);
				
				echo "Dados gravados com sucesso!"; 
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			} 
        }
        public function listarFuncionario($id_usuario)
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT *
							FROM tb_funcionario
							where id_usuario = :id_usuario";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_usuario' => $id_usuario);
				$stmt->execute($params);

				return $stmt;
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function BuscarDadosFuncionario($id_funcionario)
		{
			try{
				$con = Conecta::criarConexao();
				
				$select = "SELECT id_funcionario, ds_nome, nu_telefone
							FROM tb_funcionario 
							where id_funcionario = :id_funcionario";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_funcionario' => $id_funcionario);
				
				
				$stmt->execute($params);

				return $stmt->fetch();
				
					
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function gravarAlterarFuncionario(array $dados)
		{
			
			$ds_nome		= $dados['ds_nome'];
			$nu_telefone    = $dados['nu_telefone'];
            $id_funcionario    = $dados['id_funcionario'];
			
			
			try{
				$con = Conecta::criarConexao();
				$update = "UPDATE tb_funcionario set ds_nome = :ds_nome, nu_telefone = :nu_telefone
						WHERE id_funcionario = :id_funcionario";
				
				$stmt = $con->prepare($update);
				
				$params = array(':ds_nome' => $ds_nome, 
								':nu_telefone' => $nu_telefone,
                                ':id_funcionario' => $id_funcionario);
				$stmt->execute($params);

				
				echo "Dados alterados com sucesso!";
				
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
		public function excluirFuncionario(array $dados) 
		{
			$id_funcionario = $dados["id_funcionario"];
			try{
				$con = Conecta::criarConexao();
				
				$select = "delete from tb_funcionario
							where id_funcionario=:id_funcionario";
				
				$stmt = $con->prepare($select); 
				$params = array(':id_funcionario' => $id_funcionario);
				
				$stmt->execute($params);
				
					 
			}
			catch(exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
    			print "ERRO:".$e->getMessage();		
			}
		}
        
    }



?>