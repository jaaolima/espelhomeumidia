<?php 
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    require_once("../Classes/Ponto.php");
    require_once("../Classes/Geral.php");
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }

    $ponto = new Ponto(); 
    $geral = new Geral(); 

    $id_usuario	    = $_SESSION['id_usuario']; 
    
    if($_POST["id_busca"] === "localidade"){ 
        $localidades = "(";
        foreach($_POST["localidade"] as $indice => $valor){
            if($valor == end($_POST["localidade"])){
                $localidades .= $valor . ")";
            }else{
                $localidades .= $valor . ",";
            }
        }
        $_POST["localidade"] = $localidades;
        $retornoPonto = $ponto->FazerRelatorio($_POST, $id_usuario); 
    }else{
        $retornoPonto = $ponto->FazerRelatorio($_POST, $id_usuario); 
        $localidades = '';
    }
?> 
<input type="hidden" id="id_usuario" value="<?php echo $id_usuario;?>">
<div class="w-100 ">
    <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white " >
        <div class="card-body">
            <div>
                <div>
                    <h3 class="font-weight-bolder">
                        Resultado da Busca
                    </h3>
                </div>
                <div class="text-right">
                    <button class="btn btn-success" id="gerar_mapa">Gerar Mapa</button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal_config">Gerar PDF</button>
                </div>
                <div class="modal fade" id="modal_config" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Configuração</h3>
                            </div>
                            <div class="modal-body">
                                <form id="form_ponto">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-12 my-6">
                                                <h4>Visualização</h4>
                                                <p style="color:#808080bd;">Selecione os dados de suas mídias que aparecerão no relatório</p>
                                                <div class="d-flex w-100">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" checked value="Id" id="Id" name="st_config[]">
                                                        <label class="form-check-label" for="Id">
                                                            Id
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="checkbox" checked value="Status" id="Status" name="st_config[]">
                                                        <label class="form-check-label" for="Status">
                                                            Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="checkbox" checked value="Sentido" id="Sentido" name="st_config[]">
                                                        <label class="form-check-label" for="Sentido">
                                                            Sentido
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="checkbox" checked value="Tamanho" id="Tamanho" name="st_config[]">
                                                        <label class="form-check-label" for="Tamanho">
                                                            Tamanho
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="checkbox" checked value="Valor" id="Valor" name="st_config[]">
                                                        <label class="form-check-label" for="Valor">
                                                            Valor
                                                        </label>
                                                    </div>
                                                    
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="checkbox" checked value="Maps" id="Maps" name="st_config[]">
                                                        <label class="form-check-label" for="Maps">
                                                            Google Maps
                                                        </label>
                                                    </div>	
                                                </div>

                                            </div>
                                            <div class="separator separator-solid my-4"></div>
                                            <div class="col-12 my-6">
                                                <h4>Estilo</h4>
                                                <p style="color:#808080bd;">Selecione o estilo de como vai listar suas mídias</p>
                                                <div class="d-flex w-100">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" value="Bloco" id="Bloco" name="st_estilo">
                                                        <label class="form-check-label" for="Bloco">
                                                            Bloco
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-4">
                                                        <input class="form-check-input" type="radio" value="Lista" id="Lista" name="st_estilo">
                                                        <label class="form-check-label" for="Lista">
                                                            Lista
                                                        </label>
                                                    </div>
                                                    <!-- <div class="form-check ml-4">
                                                        <input class="form-check-input" type="radio" value="Cartao" id="Cartao" name="st_estilo">
                                                        <label class="form-check-label" for="Cartao">
                                                            Cartão
                                                        </label>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="separator separator-solid my-4"></div>
                                            <div class="col-12 my-6">
                                                <h4>Cor</h4>
                                                <p style="color:#808080bd;">Selecione a cor de sua lista</p>
                                                <div class="d-flex w-100">
                                                    <label class="form-check-inline mr-8">
                                                        <input class="form-check-input" type="radio" value="#F5F5F5" id="#F5F5F5" name="st_cor">
                                                        <div class="p-8 rounded" style="background-color:#F5F5F5;"></div>
                                                    </label>
                                                    <label class="form-check-inline mr-8">
                                                        <input class="form-check-input" type="radio" value="#eaf1f7" id="#eaf1f7" name="st_cor">
                                                        <div class="p-8 rounded" style="background-color:#eaf1f7;"></div>
                                                    </label>
                                                    <label class="form-check-inline mr-8">
                                                        <input class="form-check-input" type="radio" value="#f7ebea" id="#f7ebea" name="st_cor">
                                                        <div class="p-8 rounded" style="background-color:#f7ebea;"></div>
                                                    </label>
                                                    <label class="form-check-inline mr-8">
                                                        <input class="form-check-input" type="radio" value="#f6f7ea" id="#f6f7ea" name="st_cor">
                                                        <div class="p-8 rounded" style="background-color:#f6f7ea;"></div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id_busca" value="<?php echo $_POST["id_busca"];?>">
                                    <input type="hidden" name="ds_tipo" value="<?php echo $_POST["ds_tipo"];?>">
                                    <input type="hidden" name="localidade" value="<?php echo $localidades; ?>">
                                </form>
                            </div>
                        
                            <div class="modal-footer">
                                <button type="button" id="criar_pdf" class="btn btn-primary">Gerar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 p-6" id="table_ponto" style='overflow-x:hidden;'>
                <?php 
                    while($dados = $retornoPonto->fetch()){
                        $hoje = date('Y-m-d');
                                            
                        if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                            $status = "<span class='badge badge-xl badge-pill badge-warning mr-2 mt-1'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
                        }
                        if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                            $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Disponível</span>";
                        }
                        if($hoje < $dados["dt_inicial"]){
                            $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
                        }

                        echo "<div class='col-lg-4 col-xl-4 col-md-6 mb-8 coluna-card' >
                                <div class='card card-custom '>
                                    <!--begin::Body-->
                                    <div class='card-body text-center texto-card' style='padding: 0px !important'>
                                        <!--begin::User-->
                                        <div class='position-relative rounded' style='background:#0000001f;'>
                                            <img class='img-fluid rounded-top' src='http://espelhomeumidia.com/".$dados["ds_foto"]."' alt='image' style='height:200px;' />
                                            <div class='position-absolute'  style='top: -14px; right: -14px;'>
                                                <button data-id='".$dados['id_ponto']."' class='excluir btn btn-sm btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow' data-toggle='tooltip' data-placement='top' title='Remover do relatório'>
                                                    <i class='la la-remove text-danger' ></i> 
                                                </button>
                                            </div>
                                        </div>
                                        <!--end::User-->
                                        <!--begin::Name-->
                                        <div class='mt-8 mx-15'>
                                            <div class='d-flex ml-n8 justify-content-center mb-2'>
                                                <div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 1;overflow: hidden;'>
                                                    <p  class='m-0 text-dark font-weight-bold text-dark font-size-h4'>".$dados['ds_localidade']."(".$dados['id_ponto'].")</p>
                                                </div>	
                                            </div>	
                                            <div class='justify-content-center' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;'>
                                                <p class='texto-chumbo m-0' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>".$dados['ds_descricao']."</p>
                                            </div>																																				
                                        </div>
                                        <div class='my-8 mx-15 text-left'>
                                            <div class='d-flex ml-n8 mb-2'>
                                                <div class=''>
                                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>
                                                        <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>
                                                    </svg>
                                                </div>
                                                <div class='ml-2'>
                                                    <span class='text-dark font-weight-bold text-dark mt-1'>".$dados['ds_tipo']."</span><br>
                                                </div>													
                                            </div>
                                            <div class='d-flex ml-n8'>
                                                <div class=''>
                                                    <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>
                                                        <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>
                                                    </svg>
                                                </div>
                                                <div class='ml-2'>
                                                    <span class='text-dark font-weight-bold text-dark'>Sentido ".$dados['ds_sentido']."</span><br>
                                                </div>												
                                            </div>
                                            <div class='d-flex ml-n8'>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-aspect-ratio' viewBox='0 0 16 16'>
                                                    <path d='M0 3.5A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5v-9zM1.5 3a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z'/>
                                                    <path d='M2 4.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1H3v2.5a.5.5 0 0 1-1 0v-3zm12 7a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H13V8.5a.5.5 0 0 1 1 0v3z'/>
                                                </svg>
                                                <span class=' ml-2 font-weight-bolder text-dark'>".$dados['ds_tamanho']."</span>
                                            </div>
                                            <div class='d-flex ml-n8'>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-currency-dollar' viewBox='0 0 16 16'>
                                                    <path d='M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2.956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z'/>
                                                </svg>
                                                <span class=' ml-2 font-weight-bolder text-dark'>".$dados['nu_valor_ponto']."</span>
                                            </div>
                                            <div class='separator separator-solid my-4'></div>
                                            <div class='d-flex justify-content-center '>
                                                <div class=''>
                                                    <span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'>".$status."</span>
                                                </div>												
                                            </div>
                                        </div> 
                                        <!--end::Name-->
                                    </div>
                                    <!--end::Body-->
                                </div>
                            </div> ";
                    }
                ?>
            </div> 
        </div>
    </div>
</div>
<script>
	jQuery(document).ready(function() { 
        array_excluir = '';
        $(".excluir").on("click",function(){
            id_midia = $(this).attr("data-id");
            $(this).closest('.coluna-card').hide();
            if(array_excluir == ''){
                array_excluir = id_midia;
            }else{
                array_excluir = array_excluir + ',' + id_midia;
            }
        })
        $("#criar_pdf").on("click", function(){
            var id_busca = $('[name="id_busca"]').val();
            var ds_tipo = $('[name="ds_tipo"]').val();
            var st_estilo = $('[name="st_estilo"]:checked').val();
            var st_cor = $('[name="st_cor"]:checked').val();
            var localidade = $('[name="localidade"]').val();
            var st_config = '';
            $("[name='st_config[]']:checked").each(function(index){

                if(index == 0){
                    st_config = $(this).val();

                }else{
                    st_config = st_config + ',' + $(this).val();
                }
            })
            var url = 'appRelatorio/relatorio.php?' + $.param({id_busca: id_busca, ds_tipo: ds_tipo, st_config: st_config, localidade: localidade, array_excluir: array_excluir, st_estilo: st_estilo, st_cor: st_cor});
            window.open(url, '_blank');
        })


        $("#gerar_mapa").on("click", function(){
            var id_usuario = $('#id_usuario').val();
            var id_busca = $('[name="id_busca"]').val();
            var ds_tipo = $('[name="ds_tipo"]').val();
            var localidade = $('[name="localidade"]').val();
            var url = 'mapa_relatorio.php?' + $.param({id_busca: id_busca, ds_tipo: ds_tipo, localidade: localidade, id_usuario: id_usuario});
            window.open(url, '_blank');
        })
    })
    
</script>