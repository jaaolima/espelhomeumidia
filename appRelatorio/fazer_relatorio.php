<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    require_once("../Classes/Ponto.php");
	require_once("../Classes/Geral.php");
    $Ponto = new Ponto(); 
    $geral = new Geral(); 

    $optionsLocalidade = $Ponto->listaroptionsLocalidade();
    $optionsTipo = $geral->listarOptionsTipo(null);

?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="my-8 text-center">
        <h1 class="text-dark font-weight-bolder">Exportar</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <div class="card card-custom bg-white " >
                <div class="card-body">
                    <div class="my-3">
                        <label>Mídia</label>
                        <select name="ds_tipo" id="ds_tipo" class="form-control">
                            <option value="Todos">Todos</option>
                            <?php echo $optionsTipo; ?>
                        </select>
                    </div>
                    <div class="my-3">
                        <label>Filtro:</label>
                        <select name="ds_filtro" id="ds_filtro" class="form-control">
                            <option value="">Selecione...</option>
                            <option value="nenhum">Nenhum</option>
                            <option value="localidade">localidade</option>
                        </select>
                    </div>
                    <!-- <div class="my-3" id="status" style="display:none;">
                        <label>Status</label>
                        <div class="d-flex">
                            <div>
                                <input type="radio" name="status" id="disponivel" value="disponivel">
                                <label for="disponivel">Disponível</label>
                            </div>
                            <div class="mx-5">
                                <input type="radio" name="status" id="indisponivel" value="indisponivel">
                                <label for="indisponivel">Indisponível</label>
                            </div>
                        </div>
                        
                    </div> -->
                    <!-- <div class="my-3" id="data" style="display:none;">
                        <label>Escolha a Data:</label>
                        <div class="d-block">
                            <div class="mx-3">
                                <label for="">Data Inicial</label>
                                <input type="date" id="dt_inicial" class="form-control">
                            </div>
                            <div class="mx-3">
                                <label for="">Data Final</label>
                                <input type="date" id="dt_final" class="form-control">
                            </div></br>
                            <button id="enviar_data" class="btn btn-primary">Enviar</button>
                            

                        </div>
                        
                    </div> -->
                    <div class="my-3" id="div_filtro_nenhum" style="display:none;">
                        <button id="enviar_nenhum" class="btn btn-primary">Pesquisar</button>
                    </div>
                    <div class="my-3" id="localidade" style="display:none;">
                        <label>Escolha as localidades:</label>
                        <div class=" my-6 mx-6">
                            <table  class="table table-hover w-100" id="table_bairro">
                                <thead>
                                    <tr>
                                        <th>ID bairro</th>
                                        <th>Bairros</th>
                                        <th>Selecione</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while ($dados = $optionsLocalidade->fetch())
                                        {
                                            echo "<tr >
                                                    <td>".$dados['id_bairro']."</td>
                                                    <td>".$dados['ds_bairro']."</td>
                                                    <td><input name='localidade[]' id='".$dados["id_bairro"]."' value='".$dados['id_bairro']."' type='checkbox'></td>
                                                </tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>												
                        </div>
                        <br>
                        <button id="enviar_localidade" class="btn btn-primary">Pesquisar</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="lista" class="col-lg-8 col-sm-12" style="min-height:1000px"></div>
        
    </div>
    <script src="./assets/js/appRelatorio/relatorio.js" type="text/javascript"></script>
</body>

</html>