<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
    require_once('../assets/plugins/dompdf/autoload.inc.php');
    require_once("../Classes/Ponto.php");
    require_once("../Classes/Usuario.php");
    require_once("../Classes/Geral.php");
 
    $ponto = new Ponto(); 
    $usuario = new Usuario(); 
    $geral = new Geral(); 
    $id_usuario	    = $_SESSION['id_usuario']; 
	$dadosUsuario = $usuario->buscarUsuario($id_usuario);
    $relatorio    = $ponto->FazerRelatorio($_REQUEST, $id_usuario); 

    use Dompdf\Dompdf;

    use Dompdf\Options;

    $options = new Options();
    $options->setChroot(__DIR__);
    
    $dompdf = new Dompdf($options); 
    
    $pagina = '<!DOCTYPE html>
                <html>
                <style>
                    body{
                        font-family:Verdana, Arial, Helvetica, sans-serif;
                        // background-image: url(../assets/midia/logo.png);
                        // background-repeat: repeat;
                        // background-size: 200px 200px;
                    } ';

                    if($_REQUEST['st_estilo'] == 'Bloco'){
                        $pagina .= '@page { margin-top: 0px; margin-bottom: 0px; margin-right: 0px;margin-left: 0px; }
                                </style>';
                    }
                    if($_REQUEST['st_estilo'] == 'Lista'){
                        $pagina .= '@page { margin-top: 20px; margin-bottom: 20px; margin-right: 20px;margin-left: 20px; }
                                </style>';
                    }
                    if($_REQUEST['st_estilo'] == 'Cartao'){
                        $pagina .= '
                                    body{
                                        display: -webkit-box;
                                    }
                                    @page { margin-top: 20px; margin-bottom: 20px; margin-right: 20px;margin-left: 20px; }
                                </style>';
                    }
                   

    $hoje = date('Y-m-d');
    $st_config = explode(",", $_REQUEST["st_config"]); 
    $st_cor = $_REQUEST['st_cor'];
    while($dados = $relatorio->fetch())
    {    
        $local = explode("/", $dados["nu_localidade"]); 

        
        $id = '';
        if(in_array("Id", $st_config)){
            $id = "(".$dados["id_ponto"].")";
        }
        $status = '';
        if(in_array("Status", $st_config)){
            if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                $status = "<span style='background-color:#FFA800;border-radius: 15px;color: white;padding: 10px;font-size: 15px; text-align:center;'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
            }
            if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                $status = "<span style='background-color:#1BC5BD;border-radius: 15px;color: white;padding: 10px;font-size: 15px; text-align:center;'>Disponível</span>";
            }
            if($hoje < $dados["dt_inicial"]){
                $status = "<span style='background-color:#1BC5BD;border-radius: 15px;color: white;padding: 10px;font-size: 15px; text-align:center;'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
            }
        }
        $sentido = '';
        if(in_array("Sentido", $st_config)){
            $sentido = "<p style='margin:0px;'>Sentido ".$dados["ds_sentido"]."</p>";
        }
        $Tamanho = '';
        if(in_array("Tamanho", $st_config)){
            $Tamanho = "<p style='margin:0px;'>Tamanho: ".$dados["ds_tamanho"]."</p>";
        }
        $valor = '';
        if(in_array("Valor", $st_config)){
            if($dados["id_tipo_cobranca"] == 1){
                $valor = "<p style='margin:0px;'>Valor Mensal: ".$dados["nu_valor_ponto"]."</p>";
            }
            if($dados["id_tipo_cobranca"] == 2){
                $valor = "<p style='margin:0px;'>Valor Bisemanal: ".$dados["nu_valor_ponto"]."</p>";
            }
        } 
        $maps = '';
        if(in_array("Maps", $st_config)){
            $maps = "<p>Mapa: <a style='font-size:12px;' href='https://www.google.com.br/maps/@".$local[0].",".$local[1].",144m/data=!3m1!1e3' target='_blank'>https://www.google.com.br/maps/@".$local[0].",".$local[1].",144m/data=!3m1!1e3</a></p>";
        }

        if($_REQUEST['st_estilo'] == "Bloco"){
            $pagina .= "
                <div style='margin-left:80px;margin-right:80px;margin-bottom:20px;margin-top:20px; background-color:".$st_cor.";border-radius:15px;height:480px;'>
                    <div style='position:absolute;margin-top:0;margin-right:0;'>
                        <img src='../".$dadosUsuario["ds_logo"]."' height='50px'>
                    </div>
                    <div style=' '>
                        <img style='border-radius:15px;' src='../".$dados["ds_foto"]."' height='200px'>
                    </div>
                    <div style='text-align: end;padding-left: 10px;padding-right: 10px;padding-bottom: 5px; padding-top: 5px;'>
                        <div display:block;>
                            <h2 style='white-space: nowrap'text-overflow: ellipsis;>".$dados["ds_localidade"]." - ".$dados["ds_descricao"].$id."</h2>".$status."
                        </div>
                        <p>Tipo: ".$dados["ds_tipo"]."</p>
                        <p>Latitude/longitude: ".$dados["nu_localidade"]."</p>
                        ".$sentido."
                        ".$Tamanho."
                        ".$valor."
                        ".$maps." 
                    </div>
                </div>"; 
        }
        if($_REQUEST['st_estilo'] == "Lista"){
            $pagina .= "<div style='background-color:".$st_cor.";padding:15px;height:225px;border-bottom: 1px #8b8a8a solid;'>
                            <div style='display:flex;width: 100%;'>
                                <div style='position:absolute;margin-top:0;margin-right:0;'>
                                    <img src='../".$dadosUsuario["ds_logo"]."' height='50px'>
                                </div>
                                <div style='width:40%;'>
                                    <img class='symbol-label img-fluid rounded' style='max-height:200px;'  src='../".$dados["ds_foto"]."'>
                                </div>
                                <div style='width:60%;'>
                                    <div style='position:absolute;right:20px;'>
                                        <span  class='mb-2 m-0 ' style='font-size: 13px; display:flex;'>".$status."</span>
                                    </div>
                                    <div >
                                        <p  style='margin:0px;font-size: 20px; font-weight: 700;'>".$dados['ds_localidade']." </p>
                                        <p  style='margin:0px;font-size: 15px;'> ".$dados['ds_descricao']."</p>
                                    </div>
                                    <p  style='margin:0px;font-size: 13px;font-weight: 600;'>".$dados['ds_tipo']."</p>
                                    <p  style='margin:0px;font-size: 13px;'>".$sentido."</p>
                                    <p  style='margin:0px;font-size: 13px;'>".$dados['nu_localidade']."</p>
                                    <p  style='margin:0px;font-size: 13px;'>".$Tamanho."</p>
                                    <p  style='margin:0px;font-size: 13px;'>".$valor."</p>
                                    <p  style='margin:0px;font-size: 13px;'>".$maps."</p>
                                </div>
                            </div>
                        </div>";
        }

        if($_REQUEST['st_estilo'] == "Cartao"){
            $pagina .= "<div style='width:33%;' >
                            <a class='card card-custom card-midia'>
                                <!--begin::Body-->
                                <div class='card-body text-center texto-card' style='padding: 0px !important'>
                                    <!--begin::User-->
                                    <div class='position-relative rounded' style='background:#0000001f;'>
                                        <img class='img-fluid rounded-top' src='../".$dados["ds_foto"]."' alt='image' style='height:200px;' />
                                    </div>
                                    <!--end::User-->
                                    <!--begin::Name-->
                                    <div class='mt-8 mx-15'>
                                        <div class='d-flex justify-content-center mb-2'>
                                            <div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 1;overflow: hidden;'>
                                                <p  class='m-0 text-dark font-weight-bold text-dark font-size-h4'>".$dados['ds_localidade']."</p>
                                            </div>	
                                        </div>	
                                        <div class='justify-content-center' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;'>
                                            <p class='texto-chumbo m-0' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>".$dados['ds_descricao']."</p>
                                        </div>																																				
                                    </div>
                                    <div class='my-8 mx-15 text-left'>
                                        <div class='d-flex ml-n8 mb-2'>
                                            <div class=''>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>
                                                    <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>
                                                </svg>
                                            </div>
                                            <div class='ml-2'>
                                                <span class='text-dark font-weight-bold text-dark mt-1'>".$dados['ds_tipo']."</span><br>
                                            </div>													
                                        </div>
                                        <div class='d-flex ml-n8'>
                                            <div class=''>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>
                                                    <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>
                                                </svg>
                                            </div>
                                            <div class='ml-2'>
                                                <span class='text-dark font-weight-bold text-dark'>Sentido ".$dados['ds_sentido']."</span><br>
                                            </div>												
                                        </div>
                                        <div class='separator separator-solid my-4'></div>
                                        <div class='d-flex justify-content-center '>
                                            <div class=''>
                                                <span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'>".$status."</span>
                                            </div>												
                                        </div>
                                    </div> 
                                    <!--end::Name-->
                                </div>
                                <!--end::Body-->
                            </a>
                        </div>";
        }
        
    }
    $pagina .= '</html>';
    // echo $pagina;
    $dompdf->loadHtml($pagina);
    $dompdf->render();

    header('Content-type: application/pdf');
    echo $dompdf->output();
?> 