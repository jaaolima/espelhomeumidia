<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);

    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }

	require_once("../Classes/Cliente.php");
	require_once("../Classes/Ponto.php");
    require_once("../Classes/Documento.php");
    require_once("../Classes/Bisemana.php");


    $id_cliente = $_REQUEST["id_cliente"];
    $id_usuario = $_SESSION["id_usuario"];

    $cliente = new Cliente();
    $ponto = new Ponto();
    $documento = new Documento();
    $bisemana = new Bisemana();

    $dados = $cliente->BuscarDadosCliente($id_cliente);
    $retorno = $ponto->listarPontoCliente($id_cliente);
    $retornoBisemana = $bisemana->listarBisemanaTodas();
    $retornoTotal = $ponto->listarTotalCliente($id_cliente);
    $listaDocumentos = $documento->listarDocumentoCliente($id_cliente);
?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="my-8 text-center">
        <h1 class="mt-5 font-weight-bolder"><?php echo $dados["ds_nome"] ?></h1>
    </div>
    <div id="detalhe" class="row">
        <div class="col-6">
            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <div class="row">
                    <div class="d-block mt-2 col-md-6 col-sm-12">
                        <h3>Dados Gerais</h3>
                        <div class="d-flex">
                            <p>Responsável: </p>
                            <span class="ml-2 font-weight-bolder"><?php echo $dados["ds_nome"] ?></span>
                        </div>
                        <div class="d-flex">
                            <p>Empresa: </p>
                            <span class=" ml-2 font-weight-bolder"><?php  echo $dados["ds_empresa"] ?></span>
                        </div>
                        <div class="d-flex">
                            <p>CNPJ: </p>
                            <span class=" ml-2 font-weight-bolder"><?php  echo $dados["nu_cnpj"] ?></span>
                        </div>
                    </div>
                    <div class="d-block mt-2 col-md-6 col-sm-12">
                        <h3>Contato</h3>
                        <div class="d-flex">
                            <p>Email: </p>
                            <span class="ml-2 font-weight-bolder"><?php echo $dados["ds_email"] ?></span>
                        </div>
                        <div class="d-flex">
                            <p>Contato: </p>
                            <span class="ml-2 font-weight-bolder"><?php echo $dados["nu_telefone"] ?></span>
                        </div>
                    </div>
                    <div class="w-100 text-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_editar">Editar</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <div class="col-6">
            <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <h3 class="font-weight-bolder">Documentos</h3>
                <table class="table table-responsive d-xl-table" id="table_documento">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Documento</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while ($dadosDocs = $listaDocumentos->fetch())
                            {
                                echo "<tr>
                                        <td>".$dadosDocs['id_documento']."</td>
                                        <td><a target='_blank' href='".$dadosDocs['ds_documento']."'>".$dadosDocs['ds_nome']."</a></td>
                                        <td nowrap></td>
                                    </tr>";
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="text-right mt-2">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalDocumento">Adicionar Documento</button>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white"  >
                <div class="card-body">
                    <table class="table table-hover table-responsive d-xl-table" id="table_ponto">
                        <h3 class="font-weight-bolder">Mídias Locadas</h3>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tipo</th>
                                <th>Local</th>
                                <th>Tipo</th>
                                <th>Valor</th>
                                <th>Data Inicial</th>
                                <th>Data Final</th>
                                <th>Lat/long</th>
                                <th>Ações</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                                while ($dadosPonto = $retorno->fetch())
                                {
                                    $dt_inicial = date('d/m/Y', strtotime($dadosPonto["dt_inicial"]));
                                    $dt_final = date('d/m/Y', strtotime($dadosPonto["dt_final"]));

                                    echo "<tr>
                                            <td>".$dadosPonto['id_ponto']."</td>
                                            <td>".$dadosPonto['id_tipo']."</td>
                                            <td>".$dadosPonto['ds_localidade']."</td>
                                            <td>".$dadosPonto['ds_tipo']."</td>
                                            <td>".$dadosPonto['nu_valor']."</td>
                                            <td>".$dt_inicial."</td>
                                            <td>".$dt_final."</td>
                                            <td>".$dadosPonto['nu_localidade']." - ".$dadosPonto['ds_descricao']."</td>
                                            <td nowrap></td>
                                        </tr>";
                                    
                                }
                            ?>
                            
                        </tbody>
                    
                    </table>
                    
                    <?php 
                        $valor = 0;
                        while($dadosTotal = $retornoTotal->fetch())
                        {
                            $valor += $dadosTotal["nu_valor"];
                        } 
                    
                    ?>
                    <div class="mt-8">
                        <h3>Valor total: </h3>
                        <h3 style="color: green;"><?php echo $valor; ?></h3>
                    </div>
                    <div class="text-right mt-2">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modalPonto">Locar Mídia</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="card-title">
                        <h3>Editar</h3>
                    </div>
                </div>
                <div class="modal-header">
                    <form id="form_cliente">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Empresa <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ds_empresa" name="ds_empresa" value="<?php echo $dados['ds_empresa']?>"/>
                            </div>
                            <div class="col-md-4">
                                <label>Responsável <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ds_nome" name="ds_nome" value="<?php echo $dados['ds_nome']?>"/>
                            </div>
                            <div class="col-md-4">
                                <label>CNPJ <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="nu_cnpj" name="nu_cnpj" value="<?php echo $dados['nu_cnpj']?>"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="ds_email" name="ds_email" value="<?php echo $dados['ds_email']?>"/>
                            </div>
                            <div class="col-md-4">
                                <label>Contato <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="nu_telefone" name="nu_telefone" value="<?php echo $dados['nu_telefone']?>"/>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" id="id_cliente" name="id_cliente" value="<?php echo $dados['id_cliente']?>"/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="salvar" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </dic>
    </div>
    <div class="modal fade" id="modalDocumento" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Adicionar Documento</h5>
                </div>
                <div class="modal-body">
                    <form id="form_documento">
                        <div class="row">
                            <input type="hidden" name="st_tipo_perfil" id="st_tipo_perfil" value="1">
                            <input type="hidden" name="id_perfil" id="id_perfil" value="<?php echo $id_cliente; ?>">
                            <div class="col-6">
                                <label>Documento: <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" id="ds_documento" name="ds_documento" />
                            </div>
                            <div class="col-6">
                                <label>Nome do documento: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                            </div>
                        </div>
                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" id="adicionar_documento" class="btn btn-primary">Adicionar</button>
                </div> 
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPonto" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Adicionar Ponto</h5>
                </div>
                <div class="modal-body">
                    <form id="form_documento">
                        <div class="row">
                            <input type="hidden" name="st_tipo_perfil" id="st_tipo_perfil" value="1">
                            <input type="hidden" name="id_perfil" id="id_perfil" value="<?php echo $id_cliente; ?>">
                            <div class="col-6">
                                <label>Tipo: <span class="text-danger">*</span></label>
                                <select name="ds_tipo" id="ds_tipo" class="form-control">
                                    <option value="">Selecione...</option>
                                    <option value="1">Outdoor</option>
                                    <option value="2">Front-Light</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="table">
                        </div>
                        <div class="row">
                            <div class="col-4 ">
                                <label >Valor total contratado:<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="nu_valor" id="nu_valor">
                            </div>
                            <div class="col-4">
                                <label>Tipo:<span class="text-danger">*</span></label>
                                <select class="form-control" name="id_tipo_aluguel" id="id_tipo_aluguel">
                                    <option value="">Selecione...</option>
                                    <option value="1">Bisemanal</option>
                                    <option value="2">Mensal</option>
                                </select>
                            </div>
                        </div>
                        <div id="bisemanal" style="display:none;">
                            <div class="row my-5">
                                <table  class="table table-hover table-responsive d-xl-table" id="table_bisemana">
                                    <thead>
                                        <tr>
                                            <th>ID bisemanas</th>
                                            <th>Bisemanas Disponiveis</th>
                                            <th>Data Inicial</th>
                                            <th>Data Final</th>
                                            <th>Selecione</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while ($dados = $retornoBisemana->fetch())
                                            {

                                                $dt_inicial = date('d/m/Y', strtotime($dados["dt_inicial"]));
                                                $dt_final = date('d/m/Y', strtotime($dados["dt_final"]));


                                                echo "<tr>
                                                        <td>".$dados['id_bisemana']."</td>
                                                        <td>".$dados['ds_bisemana']."</td>
                                                        <td>".$dt_inicial."</td>
                                                        <td>".$dt_final."</td>
                                                        <td><input name='bisemana[]' id='bisemana' value='".$dados['id_bisemana']."' type='checkbox'></td>
                                                    </tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div id="mensal"  style="display:none;">
                            <div class="row">
                                <div class="col-4">
                                    <label >Data de Inicio:<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" name="dt_inicial" id="dt_inicial">
                                </div>
                            </div>
                            <table  class="table table-hover table-responsive d-xl-table" id="table_mes">
                                <thead>
                                    <tr>
                                        <th>Meses</th>
                                        <th>Selecione</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td >1 mês</td>
                                        <td><input name="meses" id="meses" value="1" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>2 meses</td>
                                        <td><input name="meses" id="meses" value="2" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>3 meses</td>
                                        <td><input name="meses" id="meses" value="3" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>4 meses</td>
                                        <td><input name="meses" id="meses" value="4" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>5 meses</td>
                                        <td><input name="meses" id="meses" value="5" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>6 meses</td>
                                        <td><input name="meses" id="meses" value="6" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>7 meses</td>
                                        <td><input name="meses" id="meses" value="7" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>8 meses</td>
                                        <td><input name="meses" id="meses" value="8" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>9 meses</td>
                                        <td><input name="meses" id="meses" value="9" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>10 meses</td>
                                        <td><input name="meses" id="meses" value="10" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>11 meses</td>
                                        <td><input name="meses" id="meses" value="11" type="radio"/></td>
                                    </tr>
                                    <tr>
                                        <td>12 meses</td>
                                        <td><input name="meses" id="meses" value="12" type="radio"/></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- <div class="d-flex">
                            <h4 class="mt-2 mr-4">Criar contrato padrão</h4>
                            <span class="switch switch-icon">
                                <label>
                                <input type="checkbox" name="select"/>
                                <span></span>
                                </label>
                            </span>
                        </div> -->
                        <input name="id_cliente" id="id_cliente" type="hidden" value="<?php echo $_REQUEST["id_cliente"]; ?>">
                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" id="adicionar_ponto" class="btn btn-primary">Adicionar</button>
                </div> 
            </div>
        </div>
    </div>
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appCliente/ver_cliente.js" type="text/javascript"></script>
    <script >
        var KTApexChartsDemo = function () {
            var _demo3 = function () {
                const apexChart = "#grafico";
                var options = {
                    series: [{
                        name: 'Net Profit',
                        data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
                    }, {
                        name: 'Revenue',
                        data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
                    }, {
                        name: 'Free Cash Flow',
                        data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                            columnWidth: '55%',
                            endingShape: 'rounded'
                        },
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
                    },
                    yaxis: {
                        title: {
                            text: '$ (thousands)'
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return "$ " + val + " thousands"
                            }
                        }
                    },
                    colors: [primary, success, warning]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }
            return {
                // public functions
                init: function () {
                    _demo3();

                }
            };
        }
        jQuery(document).ready(function () {
            KTApexChartsDemo.init();
        });
    </script>
</body>

</html>