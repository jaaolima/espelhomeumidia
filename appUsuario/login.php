<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
	require_once("../Classes/Usuario.php");
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	session_start();
	$usuario 		= new Usuario();
	$ds_usuario  	= $_REQUEST['ds_usuario']; 
	$ds_senha  		= $_REQUEST['ds_senha'];
 
	$login 			= $usuario->validaLogin($ds_usuario, $ds_senha); 
	
	if (empty($login)) 
	{
	    header('HTTP/1.1 401 Unauthorized', true, 401);
	    print "Usuário ou senha inválidos";
		$_SESSION['autenticado']		= ""; 

	}
	else 
	{
		$_SESSION['autenticado']		= "autenticado";
		$_SESSION['id_usuario']			= $login['id_usuario'];
		$_SESSION['id_empresa']			= $login['id_empresa'];
		$_SESSION['id_perfil']			= $login['id_perfil'];
		$_SESSION['ds_usuario']			= $login['ds_usuario']; 
		echo "Usuário autenticado";	
	}
	 
	
?>