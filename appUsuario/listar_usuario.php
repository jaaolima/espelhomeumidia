<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	require_once("../Classes/Cliente.php"); 

    $cliente = new Cliente();
    $id_usuario	    = $_SESSION['id_usuario'];
    $id_empresa	    = $_SESSION['id_empresa'];
    $retorno = $cliente->listarCliente($id_empresa);
?>
<!DOCTYPE html>  
<html lang="en">
    
<body>
    <div class="my-8 text-center">
        <h1 class="text-dark font-weight-bolder">Usuários</h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-dm-8 col-sm-12">
            <label style="font-size:18px;font-weight:600;" for="">Filtrar</label>
            <input type="text" class="form-control" id="filtro">
            <label class="text-gray mt-2 ml-1" for="">Pesquise por palavras chaves</label>
        </div>
        <div class="mb-3 col-lg-6 col-sm-12 justify-content-end">
            <div style="text-align:end;">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_cadastro_cliente">Novo Cliente</button>
            </div> 
        </div>
    </div>
    
    <div class="modal fade" id="modal_cadastro_cliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <div class="modal-title">
                        <h3>Novo Cliente</h3>
                    </div>
                </div>
                <div class="modal-body">
                    <form id="form_cliente">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Empresa <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ds_empresa" name="ds_empresa" />
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label>Responsável <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                            </div>
                            <div class="col-md-4">
                                <label>CNPJ <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="nu_cnpj" name="nu_cnpj" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="ds_email" name="ds_email" />
                            </div>
                            <div class="col-md-6">
                                <label>Contato <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="nu_telefone" name="nu_telefone" />
                            </div>
                        </div>
                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                        <input type="hidden" name="id_empresa" id="id_empresa" value="<?php echo $id_empresa; ?>">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="salvar" class="btn btn-primary">Adicionar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="lista"> 
        <?php
            while ($dados = $retorno->fetch())
            {

                echo "<div class='col-lg-4 col-xl-3 col-md-6 mb-8 coluna-card' >
                    <a href='appCliente/ver_cliente.php?id_cliente=".$dados['id_cliente']."' class='card card-custom card-midia'>
                        <!--begin::Body-->
                        <div class='card-body text-center texto-card' style='padding: 0px !important'>
                            <!--begin::Name-->
                            <div class='mt-8 mx-15'>
                                <div class='d-flex justify-content-center mb-2'>
                                    <div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 1;overflow: hidden;'>
                                        <p  class='m-0 text-dark font-weight-bold text-dark font-size-h4 texto-card'>".$dados['ds_empresa']."</p>
                                    </div>	
                                </div>	
                                <div class='justify-content-center' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;'>
                                    <p class='texto-chumbo m-0 text-card' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>".$dados['ds_nome']."</p>
                                </div>																																				
                            </div>
                            <div class='my-8 mx-15 text-left'>
                                <div class='d-flex ml-n8 mb-2'>
                                    <div class=''>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-wallet-fill' viewBox='0 0 16 16'>
                                            <path d='M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z'/>
                                            <path d='M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z'/>
                                        </svg>
                                    </div>
                                    <div class='ml-2'>
                                        <span class='text-dark font-weight-bold text-dark mt-1'>".$dados['ds_email']."</span><br>
                                    </div>													
                                </div>
                                <div class='d-flex ml-n8'>
                                    <div class=''>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-telephone' viewBox='0 0 16 16'>
                                            <path d='M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z'/>
                                        </svg>
                                    </div>
                                    <div class='ml-2'>
                                        <span class='text-dark font-weight-bold text-dark'>".$dados['nu_telefone']."</span><br>
                                    </div>												
                                </div>
                            </div> 
                            <!--end::Name-->
                        </div>
                        <!--end::Body-->
                    </a>
                </div> ";
            }
        ?>
    </div>
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appCliente/lista_cliente.js" type="text/javascript"></script>
    
</body>

</html>