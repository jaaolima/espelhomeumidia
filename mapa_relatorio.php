<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL); 
    date_default_timezone_set('America/Sao_Paulo');
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
    require_once("./Classes/Ponto.php");
    require_once("./Classes/Geral.php");
 
    $ponto = new Ponto(); 
    $geral = new Geral(); 
    $retornoPonto    = $ponto->FazerRelatorio($_REQUEST, $_REQUEST['id_usuario']); 

?>
<style>
    #map {
        width: 100%;
        height: 500px;
    }
    
</style>

<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <title>Espelho Meu | Dashboard</title>
    <link rel="canonical" href="https://keenthemes.com/metronic" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />	
    <link href="assets/plugins/owlCarousel/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css" />	
    <link href="assets/plugins/owlCarousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/media/logoPp.png" />
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->

</head>
<body>
    <div class="w-100">
        <div class="w-100 m-0 d-none d-lg-flex h-auto p-0 justify-content-between align-items-center"  style="background-color: #000000 !important;position:fixed; z-index:98;">
            <div>
                <h2 class="ml-3 text-white my-3" >
                    Painel Pro
                </h2>
            </div>
        </div>
        <div class="container" id="conteudo" >
            <!-- <div class="mb-8 mt-20  text-center">
                <input type="text" class="form-control" value="http://www.espelhomeumidia.com<?php echo $_SERVER["REQUEST_URI"]; ?>" readonly>
            </div> -->
            <div class="row mb-10 mt-20">
                <div class="col-12">
                    <div class="card card-custom bg-white" >
                        <div id="map" height="300px" class="w-100 rounded"></div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
    
    <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="assets/plugins/global/plugins.bundle.js"></script>
    <script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="assets/js/scripts.bundle2.min.js"></script>
    <!--begin::Page Vendors(used by this page)-->
    <script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="assets/js/pages/crud/datatables/basic/basic.js"></script>
    <script src="assets/js/datatables/lista_locacao.js"></script>
    <script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors-->
    <script src="assets/js/scripts.bundle.js"></script>
    <script src="assets/js/pages/widgets.js"></script>
    <script src="assets/js/principal.js"></script>
    <script src="../assets\plugins\owlCarousel\dist\owl.carousel.min.js"></script>
    <script src="assets/js/pages/features/charts/apexcharts.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0sGOoifQgDLzR_xYQbaGiiqXRHaJN2tM"></script>
    <script src="assets/plugins/custom/gmaps/gmaps.js"></script>
    <script>
			// The following example creates complex markers to indicate beaches near
		// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
		// to the base of the flagpole.
		jQuery(document).ready(function() { 
			if('geolocation' in navigator){
				navigator.geolocation.getCurrentPosition(function(position){
					console.log(position);
					var latitude = position.coords.latitude;
					var longitude = position.coords.longitude;
					demo3(latitude, longitude);
				}, function(error){
					if(error['code'] == 1){
						var latitude = -15.8248734;
						var longitude = -48.0607963;
						var zoom = 11;
						demo3(latitude, longitude, zoom);
					}
				})
			}
			else{
				x.innerHTML="Seu browser não suporta Geolocalização.";
			} 
			
		});
		var demo3 = function(latitude, longitude, zoom = 14) {
			var map = new GMaps({
				div: '#map',
				lat: latitude,
				lng: longitude,
				zoom: zoom
			});

			/*google.maps.event.addListener(map, 'dblclick', function(event) {
				map.addMarker({
					lat: event.latLng.lat(),
					lng: event.latLng.lng(),
					title: 'Seu ponto',
					infoWindow: {
						content: '<span style="color:#000">Aqui está o seu ponto</span>'
					}
				});	
				map.setZoom(5);
			});*/
            var bounds = new google.maps.LatLngBounds();
			<?php while($dados = $retornoPonto->fetch()){ ?>
                <?php                 
                    $latitudeElongitude = explode("/", $dados["nu_localidade"]);

                    $hoje = date('Y-m-d');     
                    if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                        $status = "<span class='badge badge-xl badge-pill badge-warning mr-2 mt-1'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
                    }
                    if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Disponível</span>";
                    }
                    if($hoje < $dados["dt_inicial"]){
                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
                    }

                    if(is_numeric($latitudeElongitude[0]) && is_numeric($latitudeElongitude[1]) && $latitudeElongitude[0] != "00.000000" && $latitudeElongitude[1] != "00.000000") :  
                ?>
                    var contentString = 
                            "<div class='row coluna-card' >"+
                                "<div class='card card-custom '>"+
                                    "<div class='card-body text-center texto-card' style='padding: 0px !important'>"+
                                        "<div class='position-relative rounded' style='background:#0000001f;'>"+
                                           " <img class='img-fluid rounded-top' src='http://espelhomeumidia.com/<?php echo $dados["ds_foto"]; ?>' alt='image' style='height:100px;' />"+
                                        "</div>"+
                                        "<div class='mt-8 mx-15'>"+
                                            "<div class='d-flex ml-n8 justify-content-center mb-2'>"+
                                               " <div class='ml-2' >"+
                                                   " <p  class='m-0 text-dark font-weight-bold text-dark font-size-h4'><?php echo $dados["ds_localidade"]; ?>(<?php echo $dados["id_ponto"]; ?>)</p>"+
                                                "</div>	"+
                                            "</div>"+
                                            "<div class='justify-content-center'>"+
                                                "<p class='texto-chumbo m-0' ><?php echo $dados["ds_descricao"]; ?></p>"+
                                            "</div>"+																																				
                                        "</div>"+
                                        "<div class='my-8 mx-15 text-left'>"+
                                            "<div class='d-flex ml-n8 mb-4'>"+
                                               " <div class=''>"+
                                                    "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>"+
                                                       " <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>"+
                                                    "</svg>"+
                                                "</div>"+
                                                "<div class='ml-2 mt-1'>"+
                                                    "<span class='text-dark font-weight-bold text-dark '><?php echo $dados["ds_tipo"]; ?></span><br>"+
                                                "</div>"+													
                                            "</div>"+
                                            "<div class='d-flex ml-n8 mb-4'>"+
                                                "<div class=''>"+
                                                    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>"+
                                                       " <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>"+
                                                    "</svg>"+
                                                "</div>"+
                                                "<div class='ml-2 mt-1'>"+
                                                    "<span class='text-dark font-weight-bold text-dark'>Sentido <?php echo $dados["ds_sentido"]; ?></span><br>"+
                                                "</div>"+												
                                            "</div>"+
                                            "<div class='d-flex ml-n8 mb-4'>"+
                                                "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-aspect-ratio' viewBox='0 0 16 16'>"+
                                                    "<path d='M0 3.5A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5v-9zM1.5 3a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z'/>"+
                                                    "<path d='M2 4.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1H3v2.5a.5.5 0 0 1-1 0v-3zm12 7a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H13V8.5a.5.5 0 0 1 1 0v3z'/>"+
                                                "</svg>"+
                                                "<span class=' ml-2 mt-1 font-weight-bolder'><?php  echo $dados['ds_tamanho'] ?></span>"+
                                            "</div>"+
                                            "<div class='d-flex ml-n8 mb-4'>"+
                                                "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-currency-dollar' viewBox='0 0 16 16'>"+
                                                    "<path d='M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2.956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z'/>"+
                                                "</svg>"+
                                                "<span class=' ml-2 mt-1 font-weight-bolder'><?php  echo $dados['nu_valor_ponto'] ?></span>"+
                                            "</div>"+
                                            "<div class='separator separator-solid my-4'></div>"+
                                            "<div class='d-flex justify-content-center '>"+
                                                "<div class=''>"+
                                                    "<span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'><?php echo $status; ?></span>"+
                                                "</div>"+												
                                            "</div>"+
                                        "</div>"+ 
                                    "</div>"+
                                "</div>"+
                            "</div>";

                    map.addMarker({
                        lat: <?php echo $latitudeElongitude[0]; ?>,
                        lng: <?php echo $latitudeElongitude[1]; ?>,
                        title: '<?php echo $dados['ds_descricao']; ?>',
                        details: {
                            database_id: 42,
                            author: 'HPNeo' 
                        },
                        infoWindow: {
                            content: contentString,
                            maxWidth: 200,
                        },
                        id: <?php echo $dados["id_ponto"]; ?>,
                    });
                    bounds.extend({lat: <?php echo $latitudeElongitude[0]; ?>, lng: <?php echo $latitudeElongitude[1]; ?>});
                <?php endif; ?>
			<?php } ?>
            map.fitBounds(bounds);
		} 

	</script>
    <!--end::Page Scripts-->
</body>

</html>