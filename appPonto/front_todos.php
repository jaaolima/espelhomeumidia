<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_save_path("/tmp");
    session_start();
    require_once('../assets/plugins/dompdf/autoload.inc.php');
    require_once("../Classes/Ponto.php");

    $ponto = new Ponto(); 
    $id_usuario	    = $_SESSION['id_usuario'];
    $listarFront = $ponto->listartodosFront($id_usuario);

    use Dompdf\Dompdf;

    use Dompdf\Options;
    
    $options = new Options();
    $options->setChroot(__DIR__);
    
    $dompdf = new Dompdf($options);
    $pagina = '<html>';
    while($dados = $listarFront->fetch())
    {    
        $pagina .= "
                    <h1 style='text-align: center;'>".$dados["ds_localidade"]."</h1>
                    <div>
                        <img src='".$dados["ds_foto"]."'>
                    </div>
                    <div style='text-align: end;'>
                        <p>Latitude/longitude:".$dados["nu_localidade"]."</p>
                        <p>Sentido:".$dados["ds_sentido"]."</p>
                        <p>Valor:".$dados["nu_valor_ponto"]."</p>
                    </div>
                    <div style=' height: 0;margin: 0.5rem 0;overflow: hidden;border-top: 1px solid #000000;'></div>";
    }
    $pagina .= '</html>';
    $dompdf->loadHtml($pagina);
    $dompdf->render();

    header('Content-type: application/pdf');
    echo $dompdf->output();
?> 