<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	require_once("../Classes/Ponto.php");
    require_once("../Classes/Bisemana.php");
    require_once("../Classes/Cliente.php");
    require_once("../Classes/Geral.php");
    require_once("../Classes/Documento.php");

    $id_ponto = $_REQUEST["id_ponto"];
    $id_tipo = $_REQUEST["id_tipo"];
    $id_usuario	    = $_SESSION['id_usuario'];

    $bisemana = new Bisemana();
    $cliente = new cliente();
    $ponto = new Ponto();
    $geral = new Geral();
    $documento = new Documento();

    $dadosPonto = $ponto->BuscarDadosPonto($id_ponto);
    $listarEmpresa = $cliente->listarOptionsEmpresa($id_usuario);
    $retorno = $bisemana->listarBisemana($id_ponto);
    $retornoData = $ponto->listarAlugado($id_ponto);
    $alugado = $ponto->listarAlugado($id_ponto);
    $listaDocumentos = $documento->listarDocumentoPonto($id_ponto);
    $optionsBairroLocalidade = $ponto->listarOptionsBairro($dadosPonto["ds_localidade"]);
    $optionsTipo = $geral->listarOptionsTipo($dadosPonto['id_tipo']);

    $latitudeElongitude = explode("/", $dadosPonto["nu_localidade"]);
    $ds_latitude = $latitudeElongitude[0];
    $ds_longitude = $latitudeElongitude[1];
?>
<head>
    <style>
        .fc-event{
            background-color: #1e90ffc2 !important;
        }
        .fc-content{
            padding: 20px !important;
        }
        .fc-content > span{
            font-size: 18px !important;
            font-weight: 800;
        }
        .select2{
            min-width: 200px !important;
        }
    </style>
</head>
<input type="hidden" id="id_ponto" name="id_ponto" value="<?php echo $id_ponto; ?>">
<input type="hidden" id="id_tipo" name="id_tipo" value="<?php echo $id_tipo; ?>">
<div class="my-8 text-center">
    <h1 class="mt-5 font-weight-bolder"><?php echo $dadosPonto["ds_localidade"] ?>(<?php echo $dadosPonto["id_ponto"] ?>)</h1>
</div>
<!-- <div class="row">
    <div class="col-6 text-center ">
        <h3 id="detalhar" class="linha-botao">Detalhes</h3> 
    </div>
    <div class="col-6 text-center">
        <h3 id="alterar" class="linha-botao">Alterar cadastro</h3>
    </div>
</div>
<div class="text-end" id="linha" >
    <div style="height:10px; width:50%;" class="bg-primary " >
    </div>
</div> -->
<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <img src="http://espelhomeumidia.com/<?php echo $dadosPonto['ds_foto']?>" class="img-fluid rounded mb-2" style="max-height: 200px;">
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="d-flex">
                        <h3 class="font-weight-bolder"><?php echo $dadosPonto["ds_localidade"] ?></h3>
                    </div>
                    <div>
                        <span class=" ml-2 "><?php  echo $dadosPonto["nu_localidade"] ?></span>
                    </div>
                    <div>
                        <span class="ml-2 "><?php echo $dadosPonto["ds_descricao"] ?></span>
                    </div>
                    <div class="d-flex mt-4">
                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>
                            <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>
                        </svg>
                        <span class="ml-2 font-weight-bolder">Sentido <?php echo $dadosPonto["ds_sentido"] ?></span>
                    </div>
                    <div class="d-flex mt-4">
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>
                            <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>
                        </svg>
                        <span class=" ml-2 font-weight-bolder"><?php  echo $dadosPonto["ds_tipo"] ?></span>
                    </div> 
                    <div class="d-flex mt-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-aspect-ratio" viewBox="0 0 16 16">
                            <path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5v-9zM1.5 3a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z"/>
                            <path d="M2 4.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1H3v2.5a.5.5 0 0 1-1 0v-3zm12 7a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H13V8.5a.5.5 0 0 1 1 0v3z"/>
                        </svg>
                        <span class=" ml-2 font-weight-bolder"><?php  echo $dadosPonto["ds_tamanho"] ?></span>
                    </div>
                    <div class="d-flex mt-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-currency-dollar" viewBox="0 0 16 16">
                            <path d="M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2.956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z"/>
                        </svg>
                        <span class=" ml-2 font-weight-bolder"><?php  echo $dadosPonto["nu_valor_ponto"] ?></span>
                    </div>
                    <div class="w-100 text-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_editar">Editar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
            <div id="mapa" style="height:200px;"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-md-12 col-sm-12">
        <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
            <div id="calendario"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="row">
            <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <h3 class="font-weight-bolder">Locação:</h3>
                <table class="table table-responsive d-xl-table" id="table_aluguel">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Empresa</th>
                            <th>Data Inicial</th>
                            <th>Data Final</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while ($dadosAlugado = $alugado->fetch())
                            {
                                $dt_inicial = date('d/m/Y', strtotime($dadosAlugado["dt_inicial"]));
                                $dt_final = date('d/m/Y', strtotime($dadosAlugado["dt_final"]));

                                if($dt_inicial <= date('d/m/Y') && date('d/m/Y') <= $dt_final){
                                    echo "<tr class='table-info'>
                                        <td>".$dadosAlugado['id_alugado']."</td>
                                        <td>".$dadosAlugado['ds_empresa']."</td>
                                        <td>".$dt_inicial."</td>
                                        <td>".$dt_final."</td>
                                        <td>nowrap</td>
                                    </tr>";
                                }
                                else{
                                    echo "<tr class=''>
                                        <td>".$dadosAlugado['id_alugado']."</td>
                                        <td>".$dadosAlugado['ds_empresa']."</td>
                                        <td>".$dt_inicial."</td>
                                        <td>".$dt_final."</td>
                                        <td>nowrap</td>
                                    </tr>";
                                }
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="text-right mt-2">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalBisemana">Adicionar Cliente</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <h3 class="font-weight-bolder">Documentos:</h3>
                <table class="table table-responsive d-xl-table" id="table_documento">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Documento</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while ($dadosDocs = $listaDocumentos->fetch())
                            {
                                echo "<tr>
                                        <td>".$dadosDocs['id_documento']."</td>
                                        <td><a target='_blank' href='".$dadosDocs['ds_documento']."'>".$dadosDocs['ds_nome']."</a></td>
                                        <td nowrap></td>
                                    </tr>";
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="text-right mt-2">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalDocumento">Adicionar Documento</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-title">
                    <h3>Editar</h3>
                </div>
            </div>
            <div class="modal-body">
                <form id="form_ponto">
                    <h3>Localização</h3>
                    <div class="row col-12"> 
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                            <label>Local: <span class="text-danger">*</span></label>
                            <select class="form-control" id="ds_localidade" name="ds_localidade"> 
                                <option value="">Selecione...</option>
                                <?php
                                    echo $optionsBairroLocalidade; 
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                            <label>Descrição: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ds_descricao" name="ds_descricao" value="<?php echo $dadosPonto["ds_descricao"] ?>"/>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                            <label>Sentido <span class="text-danger">*</span></label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Sentido </div>
                                </div>
                                <input type="text" class="form-control" id="ds_sentido" name="ds_sentido" value="<?php echo $dadosPonto["ds_sentido"] ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12">
                        <div class="form-group col-12">
                            <p>alterar a localização do ponto<span class="text-danger">*</span></p>
                            <div id="mapaEditar"></div>
                        </div>
                        <div class="col-12 row">
                            <div class="form-group col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Lat </div>
                                    </div>
                                    <input type="text" class="form-control" id="ds_latitude" name="ds_latitude"  value="<?php echo $ds_latitude ?>"/>
                                </div>
                            </div> 
                            <div class="form-group col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Long </div>
                                    </div>
                                    <input type="text" class="form-control" id="ds_longitude" name="ds_longitude"  value="<?php echo $ds_longitude ?>"/>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class='separator w-100 separator-solid my-4'></div>
                    <h3>Dados cadastrais</h3>
                    <div class="row col-12">
                        <div class="col-lg-12 col-xxl-4 col-xl-6">
                            <img src="http://espelhomeumidia.com/<?php echo $dadosPonto['ds_foto']?>" class="img-fluid rounded mb-2" style="max-height: 200px;">
                            <div>
                                <label>Troque a foto:</label>
                                <input type="file" accept="image/*" class="form-control" name="fotos[]" id="ds_foto">
                            </div>
                        </div>
                    </div>
                    <div class="row col-12">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Tipo <span class="text-danger">*</span></label>
                            <select class="form-control" name="id_tipo" id="id_tipo">
                                <?php
                                    echo $optionsTipo; 
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                            <label>Tamanho <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ds_tamanho" name="ds_tamanho" value="<?php echo $dadosPonto["ds_tamanho"] ?>" />
                        </div>
                    </div>
                    <div class='separator w-100 separator-solid my-4'></div>
                    <h3>Financeiro</h3>
                    <div class="row col-12">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <label>Tipo de Cobrança<span class="text-danger">*</span></label>
                            <select class="form-control" name="id_tipo_cobranca" id="id_tipo_cobranca">
                                <option value="">Selecione...</option>
                                <option value="1" <?php if($dadosPonto['id_tipo_cobranca'] == '1') echo "selected"; ?>>Mensal</option>
                                <option value="2" <?php if($dadosPonto['id_tipo_cobranca'] == '2') echo "selected"; ?>>Bissemanal</option> 
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                            <label>Valor <span class="text-danger">*</span></label> 
                            <input type="text" class="form-control" id="nu_valor_ponto" name="nu_valor_ponto" value="<?php echo $dadosPonto["nu_valor_ponto"] ?>"/>
                        </div>
                    </div>
                    <input type="hidden" id="id_ponto_cadastro" name="id_ponto" value="<?php echo $id_ponto?>">
                </form>
            </div>
        
            <div class="modal-footer">
                <button type="button" id="salvar" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalBisemana" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Adicionar Cliente</h5>
            </div>
            <div class="modal-body">
                <form id="form_bisemana">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Cliente:<span class="text-danger">*</span></label>
                            <select style="min-width: 100px;" class="form-control select2" name="id_cliente" id="id_cliente">
                                <?php echo $listarEmpresa ?>
                            </select>
                        </div>
                        <div class="col-md-6 ">
                            <label >Valor total contratado:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="nu_valor" id="nu_valor">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label>Tipo:<span class="text-danger">*</span></label>
                            <select class="form-control" name="id_tipo_aluguel" id="id_tipo_aluguel">
                                <option value="">Selecione...</option>
                                <option value="1">Bisemanal</option>
                                <option value="2">Mensal</option>
                            </select>
                        </div>
                    </div>
                    <div id="bisemanal" style="display:none;">
                        <div class="row my-5">
                            <table  class="table table-hover table-responsive d-xl-table" id="table_bisemana">
                                <thead>
                                    <tr>
                                        <th>ID bisemanas</th>
                                        <th>Bisemanas Disponiveis</th>
                                        <th>Data Inicial</th>
                                        <th>Data Final</th>
                                        <th>Selecione</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while ($dados = $retorno->fetch())
                                        {

                                            $dt_inicial = date('d/m/Y', strtotime($dados["dt_inicial"]));
                                            $dt_final = date('d/m/Y', strtotime($dados["dt_final"]));


                                            echo "<tr>
                                                    <td>".$dados['id_bisemana']."</td>
                                                    <td>".$dados['ds_bisemana']."</td>
                                                    <td>".$dt_inicial."</td>
                                                    <td>".$dt_final."</td>
                                                    <td><input name='bisemana[]' id='bisemana' value='".$dados['id_bisemana']."' type='checkbox'></td>
                                                </tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div id="mensal"  style="display:none;">
                        <div class="row">
                            <div class="col-4">
                                <label >Data de Inicio:<span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="dt_inicial" id="dt_inicial">
                            </div>
                        </div>
                        <table  class="table table-hover table-responsive d-xl-table" id="table_mes">
                            <thead>
                                <tr>
                                    <th>Meses</th>
                                    <th>Selecione</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td >1 mês</td>
                                    <td><input name="meses" id="meses" value="1" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>2 meses</td>
                                    <td><input name="meses" id="meses" value="2" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>3 meses</td>
                                    <td><input name="meses" id="meses" value="3" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>4 meses</td>
                                    <td><input name="meses" id="meses" value="4" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>5 meses</td>
                                    <td><input name="meses" id="meses" value="5" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>6 meses</td>
                                    <td><input name="meses" id="meses" value="6" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>7 meses</td>
                                    <td><input name="meses" id="meses" value="7" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>8 meses</td>
                                    <td><input name="meses" id="meses" value="8" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>9 meses</td>
                                    <td><input name="meses" id="meses" value="9" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>10 meses</td>
                                    <td><input name="meses" id="meses" value="10" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>11 meses</td>
                                    <td><input name="meses" id="meses" value="11" type="radio"/></td>
                                </tr>
                                <tr>
                                    <td>12 meses</td>
                                    <td><input name="meses" id="meses" value="12" type="radio"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <!-- <div class="d-flex">
                        <h4 class="mt-2 mr-4">Criar contrato padrão</h4>
                        <span class="switch switch-icon">
                            <label>
                            <input type="checkbox" name="select"/>
                            <span></span>
                            </label>
                        </span>
                    </div> -->
                    <input name="id_ponto" id="id_ponto" type="hidden" value="<?php echo $dadosPonto["id_ponto"] ?>">
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id="adicionar" class="btn btn-primary">Adicionar</button>
            </div> 
        </div>
    </div>
</div>
<div class="modal fade" id="modalDocumento" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Adicionar Documento</h5>
            </div>
            <div class="modal-body">
                <form id="form_documento">
                    <div class="row">
                        <input type="hidden" name="st_tipo_perfil" id="st_tipo_perfil" value="3">
                        <input type="hidden" name="id_perfil" id="id_perfil" value="<?php echo $id_ponto; ?>">
                        <div class="col-6">
                            <label>Documento: <span class="text-danger">*</span></label>
                            <input type="file" class="form-control" id="ds_documento" name="ds_documento" />
                        </div>
                        <div class="col-6">
                            <label>Nome do documento: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                        </div>
                    </div>
                    <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id="adicionar_documento" class="btn btn-primary">Adicionar</button>
            </div> 
        </div>
    </div>
</div>
<script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
<script src="./assets/js/appPonto/ver_ponto.js" type="text/javascript"></script>
<script src="assets/plugins/custom/leaflet/leaflet.bundle.js"></script>
<script src="assets/js/pages/features/maps/leaflet.js"></script>
<script type="text/javascript">
    var KTCalendarBackgroundEvents = function() {

        return {
            //main function to initiate the module
            init: function() {
                var todayDate = moment().startOf('day');
                var YM = todayDate.format('YYYY-MM');
                var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
                var TODAY = todayDate.format('YYYY-MM-DD');
                var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

                var calendarEl = document.getElementById('calendario');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    locale: 'pt-br',
                    plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],

                    isRTL: KTUtil.isRTL(),
                    header: {
                        left: '',
                        right: 'prev,next today',
                        center: 'title',
                    },

                    height: 800,
                    contentHeight: 780,
                    aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

                    // nowIndicator: true,
                    now: TODAY + 'T09:25:00', // just for demo

                    views: {
                        dayGridMonth: { buttonText: 'month' },
                        timeGridWeek: { buttonText: 'week' },
                        timeGridDay: { buttonText: 'day' }
                    },

                    defaultView: 'dayGridMonth',
                    defaultDate: TODAY,
                    eventLimit: true, // allow "more" link when too many events
                    businessHours: true, // display business hours
                    events: [
                        <?php


                            while($dadosData = $retornoData->fetch()){
                                
                                $dt_final = date('Y-m-d', strtotime("+1 days",strtotime($dadosData["dt_final"]))); 
                                $hoje = date('Y-m-d');
                                
                                if($hoje >= $dadosData["dt_inicial"] && $dadosData["dt_final"] >= $hoje){
                                    echo "
                                    {
                                        title: '".$dadosData["ds_empresa"]." ',
                                        start: '".$dadosData["dt_inicial"]."',
                                        description: '".$dadosData["dt_inicial"]."'/'".$dt_final."',
                                        className: 'fc-event-success fc-event-solid',
                                        end: '".$dt_final."',
                                        
                                    },";
                                }
                                else{
                                    echo "
                                    {
                                        title: '".$dadosData["ds_empresa"]." ',
                                        start: '".$dadosData["dt_inicial"]."',
                                        description: '".$dadosData["dt_inicial"]."'/'".$dt_final."',
                                        end: '".$dt_final."',
                                        className: 'fc-event-warning fc-event-solid',

                                        
                                    },";
                                }
                                $hoje = date('Y-m-d');
                                echo "
                                {
                                    start: '".$hoje."',
                                    color: '#1e8fff8a' ,
                                    rendering: 'background'
                                    
                                },";
                                

                            };


                        ?>
                    ],
                    /*eventBackgroundColor: '#46454565' ,
                    eventTextColor: 'white' ,*/
                    eventBorderColor: 'black',
                    

                    eventRender: function(info) {
                        var element = $(info.el); 
                        var elementV = info.el;

                        if (info.event.extendedProps && info.event.extendedProps.description) {
                            if (element.hasClass('fc-day-grid-event')) {
                                element.data('content', info.event.extendedProps.description);
                                element.data('placement', 'top');
                                KTApp.initPopover(element);
                            } else if (element.hasClass('fc-time-grid-event')) {
                                element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            } else if (element.find('.fc-list-item-title').lenght !== 0) {
                                element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            }
                        }
                        var event = info.event;
                        if (event.rendering == "background") {
                            elementV.append(event.title);
                        }
                    },
                });

                calendar.render();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTCalendarBackgroundEvents.init();
    });
    

    <?php 
        if(is_numeric($latitudeElongitude[0]) && is_numeric($latitudeElongitude[1]) && $latitudeElongitude[0] != "00.000000" && $latitudeElongitude[1] != "00.000000") :  
    ?>

        jQuery(document).ready(function() { 
            markers = [];

            $("#modal_editar").on("shown.bs.modal", function(){
                console.log('chegou');
            })
            var demoMap = function() {
                var map = new GMaps({
                    div: '#mapa',
                    lat: <?php echo $latitudeElongitude[0]; ?>,
                    lng: <?php echo $latitudeElongitude[1]; ?>,
                    zoom: 14
                });

                map.addMarker({
                    lat: <?php echo $latitudeElongitude[0]; ?>,
                    lng: <?php echo      $latitudeElongitude[1]; ?>,
                    title: '<?php echo $dadosPonto['ds_descricao']; ?>',
                    infoWindow: {
                        maxWidth: 200,
                    },
                    id: <?php echo $dadosPonto["id_ponto"]; ?>,
                });
            } 
            demoMap();
            
        });
        
    <?php endif; ?>

</script>