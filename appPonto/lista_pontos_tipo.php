<?php 
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);

    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal,php"){
        session_save_path("/tmp");
    }

	require_once("../Classes/Ponto.php");

    $id_tipo = $_REQUEST["id_tipo"];
    $id_usuario = $_SESSION["id_usuario"];


    $ponto = new Ponto();
    $retorno = $ponto->listarPontosTipo($id_tipo, $id_usuario);
?>

<table class="table table-hover table-responsive d-xl-table" id="table_outdoor">
    <thead>
        <tr>
            <th>ID</th>
            <th>Local</th>
            <th>Lat/long</th> 
            <th>Status</th>
            <th>Ações</th>
        </tr>

    </thead>
    <tbody>
        <?php
            while ($dados = $retorno->fetch())
            {
                $hoje = date('Y-m-d');
                
                if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                    $status = "<span class='label label-xl label-dot label-danger mr-2 mt-1'></span><p>Indisponível agora</p>";
                }
                if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]) || ($hoje < $dados["dt_inicial"]))){
                    $status = "<span class='label label-xl label-dot label-success mr-2 mt-1'></span><p>Disponível agora</p>";
                }
                
                echo "<tr>
                        <td>".$dados['id_ponto']."</td>
                        <td>
                            <div class='d-flex'>
                                <div class='d-flex'>
                                    <span class='symbol symbol-lg-50 symbol-circle symbol-40 symbol-light-success'>
                                        <img class='symbol-label img-fluid' src='http://espelhomeumidia.com/".$dados["ds_foto"]."'>
                                    </span>
                                    <div class='ml-3 mt-2'>
                                        <span>".$dados['ds_localidade']." - ".$dados['ds_descricao']."</span>	
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>".$dados['nu_localidade']."</td>
                        <td class='d-flex'>".$status."</td>
                        <td><input type='checkbox' name='pontos[]' value='".$dados["id_ponto"]."'</td>
                    </tr>";
            }
        ?>
        
    </tbody>

</table>
<script src="./assets/js/appPonto/lista_pontos_tipo.js" type="text/javascript"></script>