<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal,php"){
        session_save_path("/tmp");
    }
	require_once("../Classes/Ponto.php");
	require_once("../Classes/Geral.php");

    $ponto = new Ponto(); 
    $geral = new Geral(); 

    $id_usuario	    = $_SESSION['id_usuario'];
    $id_empresa	    = $_SESSION['id_empresa'];
    $retorno = $ponto->listarTodosPonto($id_empresa); 
    $optionsBairro = $ponto->listarOptionsBairro(null);
    $optionsTipo = $geral->listarOptionsTipo(null);

?>
<style>
    #mapCadastro {
        width: 100% !important;
        height: 300px !important;
    }

    .separator{
         border: 1px solid #f5f5f5;
    }
    
</style>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<!DOCTYPE html>
<html lang="en">
<body> 
    <div class="my-8 text-center"> 
        <h1 class="text-dark font-weight-bolder">Mídias</h1>
        
    </div>
    <div class="modal fade" id="modal_cadastro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="card-title"> 
                        <h3>Nova mídia</h3>
                    </div>
                </div>
                <div class="modal-body">
                    <form id="form_ponto">
                        <div class="row col-12">
                            <h3>Localização</h3>
                            <div class="row col-12 mt-4 ">
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Localidade <span class="text-danger">*</span></label>
                                    <select class="form-control" id="ds_localidade" name="ds_localidade"> 
                                        <option value="">Selecione...</option>
                                        <?php
                                            echo $optionsBairro; 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Descrição <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="ds_descricao" name="ds_descricao" />
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Sentido <span class="text-danger">*</span></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Sentido </div>
                                        </div>
                                        <input type="text" class="form-control" id="ds_sentido" name="ds_sentido" />
                                    </div>
                                </div>
                            </div>
                            <div class="row col-12 mt-4">
                                <div class="form-group col-12">
                                    <p>Clique duas vezes no mapa para marcar a localização do ponto<span class="text-danger">*</span></p>
                                    <div id="mapCadastro"></div>
                                </div>
                                <div class="col-12 row">
                                    <div class="form-group col-6">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Lat </div>
                                            </div>
                                            <input readonly type="text" class="form-control" id="ds_latitude" name="ds_latitude" />
                                        </div>
                                    </div> 
                                    <div class="form-group col-6">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Long </div>
                                            </div>
                                            <input readonly type="text" class="form-control" id="ds_longitude" name="ds_longitude" />
                                        </div>
                                    </div>
                                    
                                </div>
                            </div> 
                            <div class='separator w-100 separator-solid my-4'></div>
                            <h3>Dados cadastrais</h3>
                            <div class="row col-12 mt-4">
                                <div class="col-12"> 
                                    <label>Adicione uma boa foto para sua mídia <span class="text-danger">*</span></label>
                                    <input type="file" accept="image/*" class="form-control" name="fotos[]" id="ds_foto">
                                </div>
                            </div>
                            <div class="row col-12">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label>Tipo <span class="text-danger">*</span></label>
                                    <select class="form-control" name="id_tipo" id="id_tipo">
                                        <?php
                                            echo $optionsTipo; 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Tamanho <span class="text-danger">*</span></label> 
                                    <input type="text" class="form-control" id="ds_tamanho" name="ds_tamanho" />
                                </div>
                            </div>
                            <div class='separator w-100 separator-solid my-4'></div>
                            <h3>Financeiro</h3>
                            <div class="row col-12">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label>Tipo de Cobrança<span class="text-danger">*</span></label>
                                    <select class="form-control" name="id_tipo_cobranca" id="id_tipo_cobranca">
                                        <option value="">Selecione...</option>
                                        <option value="1" >Mensal</option>
                                        <option value="2">Bissemanal</option> 
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Valor <span class="text-danger">*</span></label> 
                                    <input type="text" class="form-control" id="nu_valor_ponto" name="nu_valor_ponto" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                        <input type="hidden" name="id_empresa" id="id_empresa" value="<?php echo $id_empresa; ?>">
                    </form>
                </div>
            
                <div class="modal-footer">
                    <button type="button" id="salvarPonto" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-dm-8 col-sm-12">
            <label style="font-size:18px;font-weight:600;" for="">Filtrar</label>
            <input type="text" class="form-control" id="filtro">
            <label class="text-gray mt-2 ml-1" for="">Pesquise por palavras chaves</label>
        </div>
        <div class="col-lg-6 col-dm-8 col-sm-12">
            <div style="text-align:end;">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_cadastro">Nova Mídia</button>
            </div> 
        </div>
    </div>

    <div class="row" id="lista">
        <?php 
            while($dados = $retorno->fetch()){
                $hoje = date('Y-m-d');
                                    
                if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                    $status = "<span class='badge badge-xl badge-pill badge-warning mr-2 mt-1'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
                }
                if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                    $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Disponível</span>";
                }
                if($hoje < $dados["dt_inicial"]){
                    $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
                }

                $melhor = '';
                if($dados["esta_entre_os_10_melhores"] == 'S'){
                    $melhor = "<div style='position:absolute;right:-10;top:10;'>
                                <svg data-toggle='tooltip' data-placement='top' title='Está entre as 10 mais locadas' style='color: #ff0000;' xmlns='http://www.w3.org/2000/svg' width='40' height='40' fill='currentColor' class='bi bi-fire' viewBox='0 0 40 40'>
                                    <path d='M8 16c3.314 0 6-2 6-5.5 0-1.5-.5-4-2.5-6 .25 1.5-1.25 2-1.25 2C11 4 9 .5 6 0c.357 2 .5 4-2 6-1.25 1-2 2.729-2 4.5C2 14 4.686 16 8 16Zm0-1c-1.657 0-3-1-3-2.75 0-.75.25-2 1.25-3C6.125 10 7 10.5 7 10.5c-.375-1.25.5-3.25 2-3.5-.179 1-.25 2 1 3 .625.5 1 1.364 1 2.25C11 14 9.657 15 8 15Z'/>
                                </svg>
                            </div>";
                }
                
                echo "<div class='col-lg-4 col-xl-3 col-md-6 mb-8 coluna-card' >
                        <a href='appPonto/ver_ponto.php?id_ponto=".$dados['id_ponto']."&id_tipo=".$dados["id_tipo"]."' class='card card-custom card-midia'>
                            <!--begin::Body-->
                            <div class='card-body text-center texto-card' style='padding: 0px !important'>
                                <!--begin::User-->
                                <div class='position-relative rounded' style='background:#0000001f;'>
                                    <img class='img-fluid rounded-top' src='http://espelhomeumidia.com/".$dados["ds_foto"]."' alt='image' style='height:200px;' />
                                    ".$melhor."
                                </div>
                                <!--end::User-->
                                <!--begin::Name-->
                                <div class='mt-8 mx-15'>
                                    <div class='d-flex justify-content-center mb-2'>
                                        <div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 1;overflow: hidden;'>
                                            <p  class='m-0 text-dark font-weight-bold text-dark font-size-h4'>".$dados['ds_localidade']."(".$dados['id_ponto'].")</p>
                                        </div>	
                                    </div>	
                                    <div class='justify-content-center' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;'>
                                        <p class='texto-chumbo m-0' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>".$dados['ds_descricao']."</p>
                                    </div>																																				
                                </div>
                                <div class='my-8 mx-15 text-left'>
                                    <div class='d-flex ml-n8 mb-2'>
                                        <div class=''>
                                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>
                                                <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>
                                            </svg>
                                        </div>
                                        <div class='ml-2'>
                                            <span class='text-dark font-weight-bold text-dark mt-1'>".$dados['ds_tipo']."</span><br>
                                        </div>													
                                    </div>
                                    <div class='d-flex ml-n8'>
                                        <div class=''>
                                            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>
                                                <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>
                                            </svg>
                                        </div>
                                        <div class='ml-2'>
                                            <span class='text-dark font-weight-bold text-dark'>Sentido ".$dados['ds_sentido']."</span><br>
                                        </div>												
                                    </div>
                                    <div class='d-flex ml-n8'>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-aspect-ratio' viewBox='0 0 16 16'>
                                            <path d='M0 3.5A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5v-9zM1.5 3a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z'/>
                                            <path d='M2 4.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1H3v2.5a.5.5 0 0 1-1 0v-3zm12 7a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H13V8.5a.5.5 0 0 1 1 0v3z'/>
                                        </svg>
                                        <span class=' ml-2 font-weight-bolder text-dark'>".$dados['ds_tamanho']."</span>
                                    </div>
                                    <div class='d-flex ml-n8'>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-currency-dollar' viewBox='0 0 16 16'>
                                            <path d='M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2.956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z'/>
                                        </svg>
                                        <span class=' ml-2 font-weight-bolder text-dark'>".$dados['nu_valor_ponto']."</span>
                                    </div>
                                    <div class='separator separator-solid my-4'></div>
                                    <div class='d-flex justify-content-center '>
                                        <div class=''>
                                            <span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'>".$status."</span>
                                        </div>												
                                    </div>
                                </div> 
                                <!--end::Name-->
                            </div>
                            <!--end::Body-->
                        </a>
                    </div> ";
            }
        ?>
        <!-- <div class="col-lg-12">
            <div class="card card-custom bg-white"  >
                <div class="card-header p-10">
                    <div class="col-lg-6 col-dm-8 col-sm-12">
                        <label style="font-size:18px;font-weight:600;" for="">Filtrar</label>
                        <input type="text" class="form-control" id="filtro">
                        <label class="text-gray mt-2 ml-1" for="">Pesquise por palavras chaves</label>
                    </div>
                    <div class="col-lg-6 col-dm-8 col-sm-12">
                        <div style="text-align:end;">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_cadastro">Cadastrar</button>
                        </div> 
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-responsive d-xl-table" id="table_outdoor" style='overflow-x:hidden;'>
                        <tbody>
                            <?php
                                while ($dados = $retorno->fetch())
                                {
                                    $hoje = date('Y-m-d');
                                    
                                    if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                                        $status = "<span class='badge badge-xl badge-pill badge-warning mr-2 mt-1'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
                                    }
                                    if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Disponível</span>";
                                    }
                                    if($hoje < $dados["dt_inicial"]){
                                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
                                    }

                                    $melhor = '';
                                    if($dados["esta_entre_os_10_melhores"] == 'S'){
                                        $melhor = "<div style='position:absolute;right:0;top:0;'>
                                                    <svg data-toggle='tooltip' data-placement='top' title='Está entre as 10 mais locadas' style='color: #ff0000;' xmlns='http://www.w3.org/2000/svg' width='40' height='40' fill='currentColor' class='bi bi-fire' viewBox='0 0 40 40'>
                                                        <path d='M8 16c3.314 0 6-2 6-5.5 0-1.5-.5-4-2.5-6 .25 1.5-1.25 2-1.25 2C11 4 9 .5 6 0c.357 2 .5 4-2 6-1.25 1-2 2.729-2 4.5C2 14 4.686 16 8 16Zm0-1c-1.657 0-3-1-3-2.75 0-.75.25-2 1.25-3C6.125 10 7 10.5 7 10.5c-.375-1.25.5-3.25 2-3.5-.179 1-.25 2 1 3 .625.5 1 1.364 1 2.25C11 14 9.657 15 8 15Z'/>
                                                    </svg>
                                                </div>";
                                    }
                                    
                                    echo "<tr>
                                            <td>
                                                <a href='appPonto/ver_ponto.php?id_ponto=".$dados['id_ponto']."&id_tipo=".$dados["id_tipo"]."' style='color:#3F4254;'>
                                                    <div class='d-flex row'>
                                                        <div class='col-lg-4 col-md-12 col-sm-12'>
                                                            <img class='symbol-label img-fluid rounded' style='max-height:200px;' src='http://espelhomeumidia.com/".$dados["ds_foto"]."'>
                                                        </div>
                                                        <div class='col-lg-8 col-md-12 col-sm-12'>
                                                            ".$melhor."
                                                            <div class='d-flex mb-2'>
                                                                <p class='m-0' style='font-size: 20px; font-weight: 700;'>".$dados['ds_localidade']." - </p>
                                                                <p class='m-0' style='font-size: 20px;'> ".$dados['ds_descricao']."</p>
                                                            </div>
                                                            <p class='m-0' style='font-size: 15px;font-weight: 600;'>".$dados['ds_tipo']."</p>
                                                            <p class='m-0' style='font-size: 15px;'>Sentido ".$dados['ds_sentido']."</p>
                                                            <p class='m-0' style='font-size: 15px;'>".$dados['nu_localidade']."</p>
                                                            <span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'>".$status."</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                    </tr>";
                                }
                            ?>
                            
                        </tbody>

                    </table>
                </div>
            </div>
        </div> -->
    </div> 
    

       
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appPonto/lista_ponto.js" type="text/javascript"></script>
    <!-- <script src="assets/plugins/custom/gmaps/gmaps.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0sGOoifQgDLzR_xYQbaGiiqXRHaJN2tM"></script> -->
    <!-- <script>
			// The following example creates complex markers to indicate beaches near
		// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
		// to the base of the flagpole.
		jQuery(document).ready(function() { 
			
			if('geolocation' in navigator){
				navigator.geolocation.getCurrentPosition(function(position){
					console.log(position);
					var latitude = position.coords.latitude;
					var longitude = position.coords.longitude;
					demo3(latitude, longitude);
				}, function(error){
					if(error['code'] == 1){
						var latitude = -15.8248734;
						var longitude = -48.0607963;
						var zoom = 11;
						demo3(latitude, longitude, zoom);
					}
				})
			}
			else{
				x.innerHTML="Seu browser não suporta Geolocalização.";
			} 
			
		});
		var demo3 = function(latitude, longitude, zoom = 14) {
			var map = new GMaps({
				div: '#mapCadastro',
				lat: latitude,
				lng: longitude,
				zoom: zoom,
                click: function(e) {
                    map.addMarker({
                        lat: e.latLng.lat(),
                        lng: e.latLng.lng(),
                        title: 'Seu ponto',
                        infoWindow: {
                            content: '<span style="color:#000">Aqui está o seu ponto</span>'
                        }
                    });
                },
			});

            function RemoveMarkers() {
                for (let i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
                markers = [];
            }

            function addMarker(position) {
                const marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    infoWindow: {
                        content: '<span style="color:#000">Aqui está o seu ponto!</span>'
                    },
                    icon: '../assets/media/localizacao.png'
                });

                markers.push(marker);
            }

			// google.maps.event.addListener(map, 'dblclick', function(event) {
			// 	map.addMarker({
			// 		lat: event.latLng.lat(),
			// 		lng: event.latLng.lng(),
			// 		title: 'Seu ponto',
			// 		infoWindow: {
			// 			content: '<span style="color:#000">Aqui está o seu ponto</span>'
			// 		}
			// 	});	
			// 	map.setZoom(5);
			// });

		} 

	</script> -->
    <script>
		jQuery(document).ready(function() { 
            $('[data-toggle="tooltip"]').tooltip();
            markers = [];
            function initMap() {
                const map = new google.maps.Map(document.getElementById("mapCadastro"), {
                    center: {
                    lat: -15.7750656,
                    lng: -48.0773014,
                    },
                    zoom: 12,
                    heading: 320, 
                    tilt: 47.5,
                    mapTypeId: 'roadmap'
                });
                google.maps.event.addListener(map, "dblclick", (event) => {
                    RemoveMarkers();
                    addMarker(event.latLng, map);
                    $("#ds_latitude").val(event.latLng.lat());
                    $("#ds_longitude").val(event.latLng.lng());
                });

                function RemoveMarkers() {
                    for (let i = 0; i < markers.length; i++) { 
                        markers[i].setMap(null);
                    }
                    markers = [];
                }

                function addMarker(position) {
                    const marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        infoWindow: {
                            content: '<span style="color:#000">Aqui está o seu ponto!</span>'
                        }
                    });

                    markers.push(marker);
                }

            }
            initMap();
        });
        
    </script>
</body>

</html>