<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL); 
    session_start();
    date_default_timezone_set('America/Sao_Paulo');
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	require_once("Classes/Principal.php");
	require_once("Classes/Ponto.php");
	require_once("Classes/Geral.php");
	require_once("Classes/Usuario.php");
    if($_SESSION['autenticado'] !== "autenticado") { 
		header("location: index.php");  
		exit();
	} 
    $id_usuario = $_SESSION["id_usuario"]; 
    $id_empresa = $_SESSION["id_empresa"]; 

    $ponto = new Ponto(); 
    $geral = new Geral(); 
    $usuario = new Usuario(); 
    $Principal = new Principal();
    $retornoPonto = $ponto->listarTodosPonto($id_empresa);
    $retornoTodos = $ponto->listarPontoInicial($id_empresa);
    $TotalTipos = $ponto->BuscarTotalTipo($id_empresa);
    $dadosMes = $ponto->buscarDadosDesseMes($id_empresa);
    $RetornoLocacao = $ponto->BuscarLocacaoEmpresa($id_empresa);
	$dadosMesPassado = $ponto->buscarDadosMesPassado($id_empresa);
	$dadosUsuario = $usuario->buscarUsuario($id_usuario);
?>
<style>
    #map {
        width: 100%;
        height: 300px;
    }

     /* Estilizar a barra de rolagem */
     ::-webkit-scrollbar {
      width: 10px;
    }

    /* Cor do fundo da barra de rolagem */
    ::-webkit-scrollbar-track {
      background: #f1f1f1;
    }

    /* Cor do "thumb" (a parte da barra que você arrasta) */
    ::-webkit-scrollbar-thumb {
      background: #888;
      border-radius: 15px;
    }

    /* Estilizar quando o "thumb" é ativo (quando você está arrastando) */
    ::-webkit-scrollbar-thumb:hover {
      background: #555;
    }
    .nav-item :hover > a{
        color: '#1E90FF';
    }
    .button-perfil :hover{
        border-color: "#1E90FF" !important;
        color: '#1E90FF' !important;
    }
    .card-midia:hover{
        transform: scale(1.01);
        color: initial !important;
    }
    
</style>

<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <title>Espelho Meu | Dashboard</title>
    <link rel="canonical" href="https://keenthemes.com/metronic" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />	
    <link href="assets/plugins/owlCarousel/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css" />	
    <link href="assets/plugins/owlCarousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/media/logoPp.png" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->

</head>
<body>
    <div class="w-100">
        <div class="w-100 m-0 d-none d-lg-flex h-auto p-0 justify-content-between align-items-center"  style="background-color: #000000 !important;position:fixed; z-index:98;">
            <div>
                <h2 class="ml-3 text-white" >
                    Painel Pro
                </h2>
            </div>
            <div class="mx-auto" style="padding-left:190px;">
                <ul class="nav nav-pills float-end mt-4" id="myTab" role="tablist">
                    <li class="nav-item mr-2">
                        <a href="principal.php" class="nav-link" style="font-size:16px;">Dashboard</a>
                    </li>
                    <li class="nav-item mr-2">
                        <a href="appPonto/listar_ponto.php" class="nav-link" style="font-size:16px;">Mídias</a>
                    </li>
                    <li class="nav-item mr-2">
                        <a href="appRelatorio/fazer_relatorio.php" class="nav-link" style="font-size:16px;">Exportar</a>
                    </li>
                    <li class="nav-item mr-2">
                        <a href="appRelatorio/fazer_relatorio.php" class="nav-link" style="font-size:16px;">Gráficos</a>
                    </li>
                    <!-- <li class="nav-item mr-2">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="font-size:16px;">Gerencial</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="appRelatorio/fazer_relatorio.php">Relatórios</a>
                            <a class="dropdown-item" href="#">Financeiro</a>
                        </div>
                    </li> -->
                    <li class="nav-item mr-2">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="font-size:16px;">Configurações</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="appCliente/listar_cliente.php">Clientes</a>
                        </div>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="appUsuario/listar_usuario.php">Usuários</a>
                        </div>
                    </li>
                </ul>
            </div>  
            <div class="my-2 mr-2 d-flex">    
                <div>
                    <button class="mt-3 mr-3 rounded dropdown-toggle button-perfil" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 8px; background-color: transparent; border: 1px #ffffff solid; color: #ffffff;">
                        Olá, <?php echo $dadosUsuario["ds_nome"]; ?>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="">Perfil</a>
                        <a class="dropdown-item" style="color:red;" href="appUsuario/logout.php">Sair</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="w-100 m-0 d-sm-flex d-lg-none" style="background-color: #000000 !important;position:fixed; z-index:98;">
            <ul class="nav nav-pills float-end mt-4" id="myTab" role="tablist" style="justify-content:space-around;">
                <li class="nav-item mr-2">
                    <a href="principal.php" class="nav-link p-4" style="font-size:16px;" data-toggle='tooltip' data-placement='top' title='Dashboard'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
                            <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5ZM13 7.207V13.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V7.207l5-5 5 5Z"/>
                        </svg>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a href="appPonto/listar_ponto.php" class="nav-link p-4" style="font-size:16px;" data-toggle='tooltip' data-placement='top' title='Mídias'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-display" viewBox="0 0 16 16">
                            <path d="M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z"/>
                        </svg>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a href="appRelatorio/fazer_relatorio.php" class="nav-link" style="font-size:16px;" data-toggle='tooltip' data-placement='top' title='Exportar'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-file-earmark-slides" viewBox="0 0 16 16">
                            <path d="M5 6a.5.5 0 0 0-.496.438l-.5 4A.5.5 0 0 0 4.5 11h3v2.016c-.863.055-1.5.251-1.5.484 0 .276.895.5 2 .5s2-.224 2-.5c0-.233-.637-.429-1.5-.484V11h3a.5.5 0 0 0 .496-.562l-.5-4A.5.5 0 0 0 11 6H5zm2 3.78V7.22c0-.096.106-.156.19-.106l2.13 1.279a.125.125 0 0 1 0 .214l-2.13 1.28A.125.125 0 0 1 7 9.778z"/>
                            <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z"/>
                        </svg>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a href="appRelatorio/fazer_relatorio.php" class="nav-link" style="font-size:16px;" data-toggle='tooltip' data-placement='top' title='Gráficos'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-bar-chart-line" viewBox="0 0 16 16">
                            <path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z"/>
                        </svg>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="font-size:16px;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
                            <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
                            <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
                        </svg>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="appCliente/listar_cliente.php">Clientes</a>
                    </div>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="appCliente/listar_cliente.php">Usuários</a>
                    </div>
                </li>
                <li class="nav-item mr-2">
                    <button class=" rounded dropdown-toggle button-perfil" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 8px; background-color: transparent; border: 1px #ffffff solid; color: #ffffff;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                            <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                        </svg>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="">Perfil</a>
                        <a class="dropdown-item" style="color:red;" href="appUsuario/logout.php">Sair</a>
                    </div>
                </li>

            </ul>
        </div>
        <div style="margin-top: 90px;" class="container">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-3 d-flex" style="justify-content:space-between;">
                        <div class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar mt-1" viewBox="0 0 16 16">
                                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                            </svg>
                            <p class="mb-0 mt-1 ml-2 texto-chumbo font-weight-bolder"><?php echo strftime('%A, %d de %B de %Y', strtotime('today')); ?></p>
                            <!-- <p class="ml-2" style="color: #525252;font-size: 14px;font-weight: 700;margin:0;"><?php echo date("d M, l"); ?></p> -->
                        </div>
                        <div>
                            <a href="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bell-fill" viewBox="0 0 16 16">
                                    <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" id="conteudo" >
            <div class="my-8 text-center">
                <h1 class="text-dark font-weight-bolder">Dashboard</h1>
            </div>
            <div class="row mb-10">
                <div class="col-12">
                    <div class="card card-custom bg-white" >
                        <div id="map" height="300px" class="w-100 rounded"></div>
                    </div>
                </div>
            </div>
            <div class="row mb-10 ">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="card card-custom bg-white p-10">
                        <div class="card-header justify-content-center mb-sm-2">
                            <h3 >Melhores Mídias</h3>
                            <a class="mt-1 ml-2" href="appPonto/listar_ponto.php">Ver todas</a>
                        </div>
                        <div class="owl-carousel owl-theme">
                            <?php
                                while ($dados = $retornoTodos->fetch())
                                {
                                    $hoje = date('Y-m-d');
                                    
                                    if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                                        $status = "<span class='badge badge-xl badge-pill badge-warning mr-2 mt-1'>Disponível após ".$geral->formataData($dados['dt_final'])."</span>";
                                    }
                                    if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]))){
                                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Disponível</span>";
                                    }
                                    if($hoje < $dados["dt_inicial"]){
                                        $status = "<span class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Próxima locação dia ".$geral->formataData($dados['dt_inicial'])."</span>";
                                    }
                                    
                                    echo "<div class='item'>
                                            <div class='card card-custom card-midia'>
                                                <!--begin::Body-->
                                                <div class='card-body text-center texto-card' style='padding: 0px !important'>
                                                    <!--begin::User-->
                                                    <div class='position-relative rounded' style='background:#0000001f;'>
                                                        <img class='img-fluid rounded-top' src='http://espelhomeumidia.com/".$dados["ds_foto"]."' alt='image' style='height:200px;' />
                                                        <div style='position:absolute;right:-10;top:10;'>
                                                            <svg data-toggle='tooltip' data-placement='top' title='Está entre as 10 mais locadas' style='color: #ff0000;' xmlns='http://www.w3.org/2000/svg' width='40' height='40' fill='currentColor' class='bi bi-fire' viewBox='0 0 40 40'>
                                                                <path d='M8 16c3.314 0 6-2 6-5.5 0-1.5-.5-4-2.5-6 .25 1.5-1.25 2-1.25 2C11 4 9 .5 6 0c.357 2 .5 4-2 6-1.25 1-2 2.729-2 4.5C2 14 4.686 16 8 16Zm0-1c-1.657 0-3-1-3-2.75 0-.75.25-2 1.25-3C6.125 10 7 10.5 7 10.5c-.375-1.25.5-3.25 2-3.5-.179 1-.25 2 1 3 .625.5 1 1.364 1 2.25C11 14 9.657 15 8 15Z'/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::Name-->
                                                    <div class='mt-8 mx-15'>
                                                        <div class='d-flex ml-n8 justify-content-center mb-2'>
                                                            <div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 1;overflow: hidden;'>
                                                                <a href='appPonto/ver_ponto.php?id_ponto=".$dados['id_ponto']."&id_tipo=".$dados["id_tipo"]."' class='m-0 text-dark font-weight-bold text-dark font-size-h4'>".$dados['ds_localidade']."(".$dados['id_ponto'].")</a>
                                                            </div>	
                                                        </div>	
                                                        <div class='justify-content-center' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;'>
                                                            <p class='texto-chumbo m-0' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>".$dados['ds_descricao']."</p>
                                                        </div>																																				
                                                    </div>
                                                    <div class='my-8 mx-15 text-left'>
                                                        <div class='d-flex ml-n8 mb-2'>
                                                            <div class=''>
                                                                <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-display' viewBox='0 0 16 16'>
                                                                    <path d='M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z'/>
                                                                </svg>
                                                            </div>
                                                            <div class='ml-2'>
                                                                <span class='text-dark font-weight-bold text-dark mt-1'>".$dados['ds_tipo']."</span><br>
                                                            </div>													
                                                        </div>
                                                        <div class='d-flex ml-n8'>
                                                            <div class=''>
                                                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-arrows-move' viewBox='0 0 16 16'>
                                                                    <path fill-rule='evenodd' d='M7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10zM.146 8.354a.5.5 0 0 1 0-.708l2-2a.5.5 0 1 1 .708.708L1.707 7.5H5.5a.5.5 0 0 1 0 1H1.707l1.147 1.146a.5.5 0 0 1-.708.708l-2-2zM10 8a.5.5 0 0 1 .5-.5h3.793l-1.147-1.146a.5.5 0 0 1 .708-.708l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L14.293 8.5H10.5A.5.5 0 0 1 10 8z'/>
                                                                </svg>
                                                            </div>
                                                            <div class='ml-2'>
                                                                <span class='text-dark font-weight-bold text-dark'>Sentido ".$dados['ds_sentido']."</span><br>
                                                            </div>												
                                                        </div>
                                                        <div class='d-flex ml-n8'>
                                                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' fill='currentColor' class='bi bi-aspect-ratio' viewBox='0 0 16 16'>
                                                                <path d='M0 3.5A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5v-9zM1.5 3a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z'/>
                                                                <path d='M2 4.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1H3v2.5a.5.5 0 0 1-1 0v-3zm12 7a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H13V8.5a.5.5 0 0 1 1 0v3z'/>
                                                            </svg>
                                                            <span class=' ml-2 font-weight-bolder text-dark'>".$dados['ds_tamanho']."</span>
                                                        </div>
                                                        <div class='d-flex ml-n8'>
                                                            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-currency-dollar' viewBox='0 0 16 16'>
                                                                <path d='M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2.956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z'/>
                                                            </svg>
                                                            <span class=' ml-2 font-weight-bolder text-dark'>".$dados['nu_valor_ponto']."</span>
                                                        </div>
                                                        <div class='separator separator-solid my-4'></div>
                                                        <div class='d-flex justify-content-center '>
                                                            <div class=''>
                                                                <span  class='mb-2 m-0 d-flex' style='font-size: 15px; display:flex;'>".$status."</span>
                                                            </div>												
                                                        </div>
                                                    </div> 
                                                    <!--end::Name-->
                                                </div>
                                                <!--end::Body-->
                                            </div>
                                        </div> ";
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card card-custom  p-10">
                        <div class="card-header justify-content-center">
                            <h3>Locações</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover table-responsive d-xl-table" id="table_locacao" >
                                <thead>
                                    <tr>
                                        <th>Mídia</th>
                                        <th>Cliente</th>
                                        <th>Status</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while ($dados = $RetornoLocacao->fetch())
                                        {
                                            $hoje = date('Y-m-d');
                                            
                                            if($hoje >= $dados["dt_inicial"] && $dados["dt_final"] >= $hoje){
                                                $status = "<span data-toggle='tooltip' data-placement='top' title='".$geral->formataData($dados['dt_inicial'])." até ".$geral->formataData($dados['dt_final'])."' class='badge badge-xl badge-pill badge-success mr-2 mt-1'>Ativo</span>";
                                            }
                                            if((empty($dados["dt_final"]) && empty($dados["dt_inicial"]) || ($hoje < $dados["dt_inicial"]))){
                                                $status = "<span data-toggle='tooltip' data-placement='top' title='".$geral->formataData($dados['dt_inicial'])." até ".$geral->formataData($dados['dt_final'])."' class='badge badge-xl badge-pill badge-danger mr-2 mt-1'>Inicia em ".$geral->formataData($dados['dt_inicial'])."</span>";
                                            }
                                            echo "<tr>
                                                    <td>
                                                        <div class='d-flex'>
                                                            <div class='d-flex'>
                                                                <span class='symbol symbol-lg-50 symbol-circle symbol-40 symbol-light-success'>
                                                                    <img class='symbol-label img-fluid' src='http://espelhomeumidia.com/".$dados["ds_foto"]."'>
                                                                </span>
                                                                <div class='ml-3 mt-2'>
                                                                    <a class='text-dark text-hover-primary' href='appPonto/ver_ponto.php?id_ponto=".$dados['id_ponto']."&id_tipo=".$dados["id_tipo"]."'>".$dados['ds_localidade']." - ".$dados['ds_descricao']."</a>	
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>".$dados['ds_empresa']."</td>
                                                    <td>".$status."</td>
                                                </tr>";
                                        }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>

            </div>
            <div class="row mb-10">
                <div class="col-12">
                    <div class="card card-custom bg-white p-10">
                        <div class="card-header justify-content-center">
                            <h3 >Gráficos</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5 class="mt-5">Mídias</h5>
                                    <div id="grafico" height="150px" class="w-100"></div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="mt-5">Quantidade de locações</h5>
                                    <div id="chart" height="150px" class="w-100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>       
    </div>
    <footer class="k text-center text-lg-start" style="background-color: black;">
        <!-- Copyright -->

        <div class="d-flex" style="justify-content:space-around;">
            <div class="col-lg-4 col-md-6 p-6">
                <h3 class="text-white">Fale Conosco</h3>
                <div class="rounded mt-6 bg-white mx-lg-20 p-10">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group text-left">
                                <label class="">Seu Nome</label>
                                <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group text-left">
                                <label class="">Seu Email</label>
                                <input type="text" class="form-control" id="ds_email" name="ds_email" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <label class="">Mensagem</label>
                        <textarea class="form-control" name="" id="" cols="30" rows="3"></textarea>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-primary">Enviar</button>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6 col-lg-4 p-6">
                <h3 class="text-white">Nossos Serviços</h3>
                <div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="appPonto/listar_ponto.php">Mídias</a>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="appRelatorio/fazer_relatorio.php">Exportar</a>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="appRelatorio/fazer_relatorio.php">Gráficos</a>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="appCliente/listar_cliente.php">Clientes</a>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="">Usuários</a>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <a class="font-size-h3" href="">Meu Perfil</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center p-3" style="color: white">
            © 2023 Copyright:
            <a href="https://painelpro.com/">Painelpro.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    
    <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="assets/plugins/global/plugins.bundle.js"></script>
    <script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="assets/js/scripts.bundle2.min.js"></script>
    <!--begin::Page Vendors(used by this page)-->
    <script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="assets/js/pages/crud/datatables/basic/basic.js"></script>
    <script src="assets/js/datatables/lista_locacao.js"></script>
    <script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors-->
    <script src="assets/js/scripts.bundle.js"></script>
    <script src="assets/js/pages/widgets.js"></script>
    <script src="assets/js/principal.js"></script>
    <script src="../assets\plugins\owlCarousel\dist\owl.carousel.min.js"></script>
    <script src="assets/js/pages/features/charts/apexcharts.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0sGOoifQgDLzR_xYQbaGiiqXRHaJN2tM"></script>
    <script src="assets/plugins/custom/gmaps/gmaps.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Tipo", "Quantidade", { role: "style" } ],
                ["Outdoor", <?php echo $TotalTipos[0]["id_ponto"]?> , "#1E90FF"],
                ["Front-Light", <?php echo $TotalTipos[1]["id_ponto"]?>, "#1E90FF"],
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                            { calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation" },
                            2]);


            var options = {
                chart: {
                    height: 150,
                    bar: {groupWidth: "50%"},
                    legend: { position: "none" },
                }
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("grafico"));
            chart.draw(view, options);
        }
        google.charts.load('current', {'packages':['bar']})
        google.charts.setOnLoadCallback(drawMaterial);
        function drawMaterial() {
            var data = google.visualization.arrayToDataTable([
                ['', 'Outdoor', 'Front-light'],
                ['Mês passado', <?php echo $dadosMesPassado[0]; ?>, <?php echo $dadosMesPassado[1]; ?>],
                ['Esse mês',<?php echo $dadosMes[0]; ?>, <?php echo $dadosMes[1]; ?>]
            ]);

            var options = {
                chart: {
                    height: 150,
                    bar: {groupWidth: "50%"},
                    legend: { position: "none" },
                }
            };

            var chart = new google.charts.Bar(document.getElementById('chart'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
    <script>
			// The following example creates complex markers to indicate beaches near
		// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
		// to the base of the flagpole.
		jQuery(document).ready(function() { 
            $(".owl-carousel").owlCarousel({
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                stagePadding: 50,
                items:4,
                center:true,
                loop:true,
                dots:false,
                margin:30,
                responsive: {
                    900:{
                        items:1
                    },
                    1800:{
                        items:2
                    }
                }

            });
            $('[data-toggle="tooltip"]').tooltip();
			
			if('geolocation' in navigator){
				navigator.geolocation.getCurrentPosition(function(position){
					console.log(position);
					var latitude = position.coords.latitude;
					var longitude = position.coords.longitude;
					demo3(latitude, longitude);
				}, function(error){
					if(error['code'] == 1){
						var latitude = -15.8248734;
						var longitude = -48.0607963;
						var zoom = 11;
						demo3(latitude, longitude, zoom);
					}
				})
			}
			else{
				x.innerHTML="Seu browser não suporta Geolocalização.";
			} 
			
		});
		var demo3 = function(latitude, longitude, zoom = 14) {
			var map = new GMaps({
				div: '#map',
				lat: latitude,
				lng: longitude,
				zoom: zoom
			});

			/*google.maps.event.addListener(map, 'dblclick', function(event) {
				map.addMarker({
					lat: event.latLng.lat(),
					lng: event.latLng.lng(),
					title: 'Seu ponto',
					infoWindow: {
						content: '<span style="color:#000">Aqui está o seu ponto</span>'
					}
				});	
				map.setZoom(5);
			});*/
            var bounds = new google.maps.LatLngBounds();
			<?php while($dados = $retornoPonto->fetch()){ ?>
                <?php                 
                    $latitudeElongitude = explode("/", $dados["nu_localidade"]);
                    if(is_numeric($latitudeElongitude[0]) && is_numeric($latitudeElongitude[1]) && $latitudeElongitude[0] != "00.000000" && $latitudeElongitude[1] != "00.000000") :  
                ?>
                    var contentString = 
                        "<div class='card card-custom'>"+
                            "<div class='card-body text-center' style='padding: 0px !important'>"+
                                "<div class='position-relative' >"+
                                    "<img class='img-fluid w-100 rounded-top' src='http://espelhomeumidia.com/<?php echo $dados["ds_foto"]; ?>' style='height:60px;' alt='image'/>"+
                                "</div>"+
                                "<div class='text-left'>"+
                                    "<div class='d-flex ml-n8 mt-2'>"+
                                        "<div class='mt-1'>"+
                                            "<svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>"+
                                                "<path d='M19.7273 10.3636C19.7273 16.0909 12.3636 21 12.3636 21C12.3636 21 5 16.0909 5 10.3636C5 8.41068 5.77581 6.53771 7.15676 5.15676C8.53771 3.77581 10.4107 3 12.3636 3C14.3166 3 16.1896 3.77581 17.5705 5.15676C18.9515 6.53771 19.7273 8.41068 19.7273 10.3636Z' stroke='#57616A' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'/>"+
                                                "<path d='M12.3636 12.8183C13.7192 12.8183 14.8181 11.7193 14.8181 10.3637C14.8181 9.00812 13.7192 7.90918 12.3636 7.90918C11.008 7.90918 9.90906 9.00812 9.90906 10.3637C9.90906 11.7193 11.008 12.8183 12.3636 12.8183Z' stroke='#57616A' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'/>"+
                                            "</svg>"+
                                        "</div>"+
                                        "<div class='ml-2'  style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>"+
                                            "<a href='appPonto/ver_ponto.php?id_ponto=<?php echo $dados['id_ponto'];?>&id_tipo=<?php echo $dados['id_tipo'];?>' class='m-0 text-dark font-weight-bold text-dark font-size-h4'><?php echo $dados['ds_localidade'];?></a>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div class='mb-6' style='text-overflow: ellipsis;-webkit-box-orient: vertical;display: -webkit-box;-webkit-line-clamp: 2;overflow: hidden;'>"+
                                        "<p class='texto-chumbo font-size-h6 m-0'><?php echo $dados['ds_descricao'];?></p>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='separator separator-solid'></div>"+
                                "<div class='mt-2 text-right'>"+
                                    "<a href='appPonto/ver_ponto.php?id_ponto=<?php echo $dados['id_ponto'];?>&id_tipo=<?php echo $dados['id_tipo'];?>' class='text-primary'>Ver detalhes"+
                                    "</a>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
                

                    map.addMarker({
                        lat: <?php echo $latitudeElongitude[0]; ?>,
                        lng: <?php echo $latitudeElongitude[1]; ?>,
                        title: '<?php echo $dados['ds_descricao']; ?>',
                        details: {
                            database_id: 42,
                            author: 'HPNeo' 
                        },
                        infoWindow: {
                            content: contentString,
                            maxWidth: 200,
                        },
                        id: <?php echo $dados["id_ponto"]; ?>,
                    });
                    bounds.extend({lat: <?php echo $latitudeElongitude[0]; ?>, lng: <?php echo $latitudeElongitude[1]; ?>});
                <?php endif; ?>
			<?php } ?>
            map.fitBounds(bounds);
		} 

	</script>
    <!--end::Page Scripts-->
</body>

</html>