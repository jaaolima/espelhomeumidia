$(document).ready(function(){
    $('[name="ds_tipo"]').change(function(){
        if($(this).val() === "Outdoor"){
            $("#bisemana").show();
        }
        if($(this).val() !== "Outdoor"){
            $("#bisemana").hide();
        }
    });
    $('[name="ds_filtro"]').change(function(){
        if($(this).val() === "localidade"){
            $("#localidade").show(); 
        }else{
            $("#localidade").hide(); 
        }
        if($(this).val() === "nenhum"){
            $("#div_filtro_nenhum").show(); 
        }else{
            $("#div_filtro_nenhum").hide(); 
        }
    });


    $('#PDF').on("click", function(){
        tipo_filtro = $('[name="ds_filtro"]').val();
        if(tipo_filtro === "status"){
            id_status = $(this).val();
            ds_tipo = $('[name="ds_tipo"]').val();
            $.ajax({
                url: 'appRelatorio/relatorio.php' 
                , data: {id_busca: 'status', id_status: id_status, ds_tipo: ds_tipo}
                , type: 'post'
            });	
        }
    });

    $("#enviar_data").on("click", function(){
        dt_inicial = $("#dt_inicial").val();
        dt_final = $("#dt_final").val();
        ds_tipo = $('[name="ds_tipo"]').val();
        $.ajax({
            url: 'appRelatorio/listar_relatorio.php' 
            , data: {id_busca: 'data', dt_inicial: dt_inicial,dt_final: dt_final,  ds_tipo: ds_tipo}
            , type: 'post'
            , success: function(html) {
                $("#lista").html(html);
                $("#lista").slideDown(); 
            } 
            , error: function (data) {
                $("#lista").slideUp();
                swal("Erro", data.responseText, "error");
            }
        });	
    });
    $("#enviar_nenhum").on("click", function(){
        ds_tipo = $('[name="ds_tipo"]').val();
        $.ajax({
            url: 'appRelatorio/listar_relatorio.php' 
            , data: {id_busca: 'nenhum', ds_tipo: ds_tipo}
            , type: 'post'
            , success: function(html) {
                $("#lista").html(html);
                $("#lista").slideDown(); 
            }
            , error: function (data) {
                $("#lista").slideUp();
                swal("Erro", data.responseText, "error");
            }
        });	
    });
    var localidade = new Array();
    $("input[name='localidade[]']").on("click", function(){
        if(localidade.includes($(this).val())){
            localidade.splice(localidade.indexOf($(this).val()), 1); 
        }else{
            localidade.push($(this).val());
        }
        console.log(localidade);
    })
    
    $("#enviar_localidade").on("click", function(){
        ds_tipo = $('[name="ds_tipo"]').val();
        $.ajax({
            url: 'appRelatorio/listar_relatorio.php' 
            , data: {id_busca: 'localidade', localidade: localidade,  ds_tipo: ds_tipo}
            , type: 'post'
            , success: function(html) {
                $("#lista").html(html);
                $("#lista").slideDown(); 
            }
            , error: function (data) {
                $("#lista").slideUp();
                swal("Erro", data.responseText, "error");
            }
        });	
    });

    $('[name="status"]').change(function(){
        id_status = $(this).val();
        ds_tipo = $('[name="ds_tipo"]').val();
        $.ajax({
            url: 'appRelatorio/listar_relatorio.php' 
            , data: {id_busca: 'status', id_status: id_status, ds_tipo: ds_tipo}
            , type: 'post'
            , success: function(html) {
                $("#lista").html(html);
                $("#lista").slideDown(); 
            }
            , error: function (data) {
                $("#lista").slideUp();
                swal("Erro", data.responseText, "error");
            }
        });	
    });
});
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table_bairro');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true,
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>> 
			<'row'<'col-sm-12 col-md-5'><'col-sm-12 col-md-7 dataTables_pager'p>>`,

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[0, 'asc']],


			columnDefs: [

				{
					targets: 0,
					visible: false
				},
				
				
			],
		});
	

		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();


jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});