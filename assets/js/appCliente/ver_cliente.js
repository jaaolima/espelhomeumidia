$(document).ready(function() {
	    
	id_cliente = $("#id_cliente").val();
	
	$("#salvar").on("click", function(e){  
		if(validarCliente()) 
		{ 
			$.ajax({
		        url: 'appCliente/gravar_alterar_cliente.php'
				, data: $("#form_cliente").serialize()
		        , type: 'post'
		        , success: function(html) {
		        	swal.fire({
		                position: 'top-right',
		                type: 'success', 
		                title: html,
		                showConfirmButton: true
		            });	
					$('#modal_editar').modal('hide');
					$(document).on('hidden.bs.modal','#modal_editar', function () {
						redirectTo("appCliente/ver_cliente.php?id_cliente="+id_cliente); 
					});
					  
		        }
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
});

$("#adicionar_documento").on("click", function(e){ 
	if(validarDocumento())
	{ 	
		id_ponto = $("#id_ponto").val();
		id_tipo = $("#id_tipo").val();
		var form = $("#form_documento").get(0); 
		$.ajax({
			
			url: 'appDocumento/gravar_documento.php'
			, data: $("#form_documento").serialize()
			, type: 'post'
			, data: new FormData(form)
			, mimeType: 'multipart/form-data'  
			, processData: false
			, contentType: false
			, success: function sucesso(html) {
				$('#modalDocumento').modal('hide');
				$(document).on('hidden.bs.modal','#modalDocumento', function () {
					redirectTo("appCliente/ver_cliente.php?id_cliente="+id_cliente);
				});
				
			}
			
			, error: function (data) {
				swal.fire("Erro", data.responseText, "error"); 
			}
		});		 
	}	
});
$("#ds_tipo").on("change", function(e){ 
	id_tipo = $(this).val();
	$.ajax({
		url: 'appPonto/lista_pontos_tipo.php'
		, data: {id_tipo: id_tipo}
		, type: 'post'
		, success: function(html) {
			$("#table").html(html); 
		}
		, error: function (data) {
			swal.fire("Erro", data.responseText, "error");
		}
	});		
});

$("#id_tipo_aluguel").change(function(){
	if($(this).val() == 1){
		$("#bisemanal").show();
		$("#mensal").hide();
	}
	if($(this).val() == 2){
		$("#mensal").show();
		$("#bisemanal").hide();
	}
})

$("#adicionar_ponto").on("click", function(e){ 
	if(validarAlugado())
	{
		id_cliente = $("#id_cliente").val();
		id_tipo = $("#id_tipo").val();
		$.ajax({
			url: 'appPonto/gravar_pontos_cliente.php'
			, data: {forms: $("#form_bisemana").serialize(), pontos: pontos}
			, type: 'post'
			, success: function(html) {
				$('#modalPonto').modal('hide');
				$(document).on('hidden.bs.modal','#modalPonto', function () {
					redirectTo("appCliente/ver_cliente.php?id_cliente="+id_cliente);
				});
			}
			, error: function (data) { 
				swal.fire("Erro", data.responseText, "error");  
			}
		});		
		
	}
});	

var pontos = new Array();
$("input[name='pontos[]']").on("click", function(){
	if(pontos.includes($(this).val())){
		pontos.splice(pontos.indexOf($(this).val()), 1); 
	}else{
		pontos.push($(this).val());
	}
})

$("#alterar").on("click", function(){
	$("#cadastro").show()
	$("#detalhe").hide()
	$("#linha").css("text-align", "-webkit-right")
})
$("#detalhar").on("click", function(){
	$("#detalhe").show()
	$("#cadastro").hide()
	$("#linha").css("text-align", "-webkit-left") 
}) 

$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});
$("#nu_cnpj").inputmask({
	"mask": "99.999.999/9999-99", 
	autoUnmask: false,
});
$("#nu_cep").inputmask({
	"mask": "99.999-999",
	autoUnmask: false,
});

$("#nu_valor").inputmask({
	'alias': 'numeric',
	'groupSeparator': '.',
	'autoGroup': true,
	'prefix': 'R$ ',
});

function fecharModal(){

	$('#modalDocumento').modal('hide');

	if(!$('#modalDocumento').hasClass("show")){
		console.log("não tem");
		return true;
	}
	
}


function validarDocumento()
{
	if($("#st_tipo_perfil option:selected").val() == "")
	{
		$("#st_tipo_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#st_tipo_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#st_tipo_perfil").removeClass("is-invalid");	
		$("#st_tipo_perfil").addClass("is-valid");
	}

	if($("#id_perfil option:selected").val() == "")
	{
		$("#id_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#id_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_perfil").removeClass("is-invalid");	
		$("#id_perfil").addClass("is-valid");
	}
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o nome do documento", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}

	return true;
}

function validarAlugado()
{
	if($("#nu_valor").val() == "")
	{
		$("#nu_valor").focus();
		swal.fire("Erro", "Preencha o valor", "error");
		$("#nu_valor").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_valor").removeClass("is-invalid");	
		$("#nu_valor").addClass("is-valid");
	}

	if($("#id_tipo_aluguel option:selected").val() == "")
	{
		$("#id_tipo_aluguel").focus();
		swal.fire("Erro", "Selecione um tipo de aluguel", "error");
		$("#id_tipo_aluguel").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo_aluguel").removeClass("is-invalid");	 
		$("#id_tipo_aluguel").addClass("is-valid");
	}

	if($("#id_tipo_aluguel option:selected").val() === "1"){

		if ($('input[name="bisemana[]"]:checked').length == 0)
		{
			$("input[name='bisemana[]']").focus();
			swal.fire("Aviso", "Selecione uma das opções", "warning");      
			return false;	
		}
	}

	if($("#id_tipo_aluguel option:selected").val() === "2"){

		if($("#dt_inicial").val() == "")
		{
			$("#dt_inicial").focus();
			swal.fire("Erro", "Preencha a Data inicial", "error");
			$("#dt_inicial").addClass("is-invalid");
			return false;	
		}
		else
		{
			$("#dt_inicial").removeClass("is-invalid");	
			$("#dt_inicial").addClass("is-valid");
		}

		if ($('input[name="meses"]:checked').length == 0)
		{
			$("input[name='meses']").focus();
			swal.fire("Aviso", "Selecione uma das opções", "warning");      
			return false;	
		}

	}


	return true;
}

function validarCliente()
{
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		/*swal.fire("Erro", "Preencha a descrição", "error");*/
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}
	if($("#ds_empresa").val() == "")
	{
		$("#ds_empresa").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#ds_empresa").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_empresa").removeClass("is-invalid");	
		$("#ds_empresa").addClass("is-valid");
	}
	if($("#nu_cnpj").val() == "")
	{
		$("#nu_cnpj").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#nu_cnpj").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_cnpj").removeClass("is-invalid");	
		$("#nu_cnpj").addClass("is-valid");
	}
	if($("#nu_telefone").val() == "")
	{
		$("#nu_telefone").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#nu_telefone").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_telefone").removeClass("is-invalid");	
		$("#nu_telefone").addClass("is-valid");
	}
	if($("#ds_email").val() == "")
	{
		$("#ds_email").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#ds_email").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_email").removeClass("is-invalid");	
		$("#ds_email").addClass("is-valid");
	}

	return true;
}
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table_ponto');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					
					render: function(data, type, full, meta) {
						return `
                        
                         <a href="appPonto/ver_ponto.php?id_ponto=`+full[0]+`&id_tipo=`+full[1]+`"" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Visualizar Cadastro">
                          <i class="la la-edit"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				{
					targets: 1,
					visible: false
				},
				
				
			],
		});
	

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	var initTable2 = function() {
		var table = $('#table_documento');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-documento="`+full[0]+`" >
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_documento = $(this).data("documento");
			swal.fire({ 
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Documento?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appDocumento/excluir_documento.php'
				        , type: 'post'
				        , data: {id_documento : id_documento}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							var id_cliente = $("#id_cliente").val();
							redirectTo("appCliente/ver_cliente.php?id_cliente="+id_cliente);					
				        }
				    });
	                
	            }
	        });
			
		}); 

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};


	return {

		//main function to initiate the module
		init: function() {
			initTable1(); 
			initTable2(); 
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});