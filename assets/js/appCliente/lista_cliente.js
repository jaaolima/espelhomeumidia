$(document).ready(function() {
	    
	$("#salvar").on("click", function(e){ 

		
		if(validarCliente())
		{ 
			$.ajax({
		        url: 'appCliente/gravar_cliente.php'
				, data: $("#form_cliente").serialize()
		        , type: 'post'
		        , success: function(html) {
					swal.fire({
		                position: 'top-right',
		                type: 'success',
		                title: html,
		                showConfirmButton: true
		            });
					$('#modal_cadastro_cliente').modal('hide');
					$(document).on('hidden.bs.modal','#modal_cadastro_cliente', function () {
						redirectTo("appCliente/listar_cliente.php");      
					});
		        }
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
			
		}	
	});
	$('#filtro').keyup(function () {
		var texto = $(this).val().toLowerCase().split(' ');
		$('#lista .texto-card').each(function () {
			for (let i = 0; i < texto.length; i++) {
				const element = texto[i];
				var linha = $(this).text().toLowerCase(); 
				if (linha.indexOf(element) === -1) {
					$(this).closest('.coluna-card').hide();
					return true;
				} else {
					$(this).closest('.coluna-card').show(); 
				}
			}
		 
		});
	});
	$("#adicionar").on("click", function(){
		$("#cadastro").show()
		$("#lista").hide()
		$("#linha").css("text-align", "-webkit-right")
	})
	$("#listar").on("click", function(){
		$("#lista").show()
		$("#cadastro").hide()
		$("#linha").css("text-align", "-webkit-left")
	})
});

$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});
$("#nu_cnpj").inputmask({
	"mask": "99.999.999/9999-99",
	autoUnmask: false,
});
$("#nu_cep").inputmask({
	"mask": "99.999-999",
	autoUnmask: false,
});

function validarCliente()
{
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o responsável", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}
	if($("#ds_empresa").val() == "")
	{
		$("#ds_empresa").focus();
		swal.fire("Erro", "Preencha a empresa", "error");
		$("#ds_empresa").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_empresa").removeClass("is-invalid");	
		$("#ds_empresa").addClass("is-valid");
	}
	if($("#nu_cnpj").val() == "")
	{
		$("#nu_cnpj").focus();
		swal.fire("Erro", "Preencha o CNPJ", "error");
		$("#nu_cnpj").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_cnpj").removeClass("is-invalid");	
		$("#nu_cnpj").addClass("is-valid");
	}
	if($("#ds_email").val() == "")
	{
		$("#ds_email").focus();
		swal.fire("Erro", "Preencha o E-mail", "error");
		$("#ds_email").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_email").removeClass("is-invalid");	
		$("#ds_email").addClass("is-valid");
	}
	if($("#nu_telefone").val() == "")
	{
		$("#nu_telefone").focus();
		swal.fire("Erro", "Preencha o telefone", "error");
		$("#nu_telefone").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_telefone").removeClass("is-invalid");	
		$("#nu_telefone").addClass("is-valid");
	}
	return true;
}
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table_cliente');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true,
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
                         <a href="appCliente/ver_cliente.php?id_cliente=`+full[0]+`"" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Visualizar Cadastro">
                          <i class="la la-edit"></i>
                        </a>
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-cliente="`+full[0]+`" > 
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_cliente = $(this).data("cliente");
			swal.fire({
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Cliente?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appCliente/excluir_cliente.php'
				        , type: 'post'
				        , data: {id_cliente : id_cliente}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							redirectTo("appCliente/listar_cliente.php");				
				        }
				    });
	                
	            }
	        });
			
		});
	
 
		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	

		/*table.on('click', '#btn-frequencia', function() {
			
			var id_aula = $(this).data("aula");

			redirectTo("appDiario/cadastro_frequencia.php?id_aula="+id_aula);
			
		});*/

		table.on('click', '#btn-editar', function() {

			redirectTo("appPonto/alterar_cadastro");
			
		});

		/*table.on('click', '#btn-excluir', function() {
			var id_aula = $(this).data("aula");

			swal({
	            title: 'Tem certeza?',
	            text: "Desejar excluir a aula e todas as frequências associadas?",
	            type: 'warning',
	            showCancelButton: true,
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appDiario/excluir_aula.php'
				        , type: 'post'
				        , data: {id_aula : id_aula}
				        , success: function(html) {
							swal('Pronto!',html,'success');
							$.ajax({
						        url: 'appDiario/listar_aula.php'
						        , type: 'post'
						        , data: $("#busca_aula").serialize()
						        , success: function(html) {
						        	$("#lista").html(html);
						        }
						        , error: function(xhr, status, error) {
								  	swal("Erro", xhr.responseText, "error");
								}
						    });						
				        }
				    });
	                
	            }
	        });
			
		}); */
		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});