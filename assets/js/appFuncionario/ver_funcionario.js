$(document).ready(function() {
	    
	id_funcionario = $("#id_funcionario").val();
	
	$("#salvar").on("click", function(e){ 
		if(validar())
		{ 
			$.ajax({ 
		        url: 'appFuncionario/gravar_alterar_funcionario.php'
				, data: $("#form_cliente").serialize()
		        , type: 'post'
		        , success: function(html) {
		        	swal.fire({
		                position: 'top-right',
		                type: 'success', 
		                title: html,
		                showConfirmButton: true 
		            });	
					$('#modal').modal('hide');
					$(document).on('hidden.bs.modal','#modal', function () {
						redirectTo("appFuncionario/ver_funcionario.php?id_funcionario="+id_funcionario); 
					});
					  
		        }
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
	$("#adicionar_documento").on("click", function(e){ 
		if(validarDocumento())
		{ 	
			var form = $("#form_documento").get(0); 
			$.ajax({
				
				url: 'appDocumento/gravar_documento.php'
				, data: $("#form_documento").serialize()
				, type: 'post'
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
				, success: function sucesso(html) {

					$('#modalDocumento').modal('hide');
					$(document).on('hidden.bs.modal','#modalDocumento', function () {
						redirectTo("appFuncionario/ver_funcionario.php?id_funcionario="+id_funcionario); 
					});
					
				}
				
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
			});		 
		}	
	});
}); 



$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});

function fecharModal(){
	$("#modal").modal('hide');

	$('#modalDocumento').modal('hide');

	if(!$('#modalDocumento').hasClass("show")){
		console.log("não tem");
		return true;
	}
}


function validarDocumento()
{
	if($("#st_tipo_perfil option:selected").val() == "")
	{
		$("#st_tipo_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#st_tipo_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#st_tipo_perfil").removeClass("is-invalid");	
		$("#st_tipo_perfil").addClass("is-valid");
	}

	if($("#id_perfil option:selected").val() == "")
	{
		$("#id_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#id_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_perfil").removeClass("is-invalid");	
		$("#id_perfil").addClass("is-valid");
	}
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o nome do documento", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}

	return true;
}

function validar()
{
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		/*swal.fire("Erro", "Preencha a descrição", "error");*/
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}
	if($("#nu_telefone").val() == "")
	{
		$("#nu_telefone").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#nu_telefone").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_telefone").removeClass("is-invalid");	
		$("#nu_telefone").addClass("is-valid");
	}

	return true;
}
var DatatablesBasicBasic = function() {

	var initTable2 = function() {
		var table = $('#table_documento');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-documento="`+full[0]+`" >
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_documento = $(this).data("documento");
			swal.fire({ 
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Documento?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appDocumento/excluir_documento.php'
				        , type: 'post'
				        , data: {id_documento : id_documento}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							var id_funcionario = $("#id_funcionario").val();
							redirectTo("appFuncionario/ver_funcionario.php?id_funcionario="+id_funcionario); 					
				        }
				    });
	                
	            }
	        });
			
		}); 

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};


	return {

		//main function to initiate the module
		init: function() {
			initTable2(); 
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});