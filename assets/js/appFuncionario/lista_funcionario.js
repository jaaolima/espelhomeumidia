$(document).ready(function() {
	    
	$("#salvar").on("click", function(e){ 
		if(validar())
		{ 	
			var form = $("#form_ponto").get(0); 
			$.ajax({
				
		        url: 'appFuncionario/gravar_funcionario.php'
				, data: $("#form_ponto").serialize()
		        , type: 'post'
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
		        , success: function sucesso(html) {

					swal.fire({
						position: 'top-right',
						type: 'success',
						title: html,
						showConfirmButton: true
					});
					redirectTo("appFuncionario/listar_funcionario.php");
					
		        }
				
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
	$("#adicionar").on("click", function(){
		$("#cadastro").show()
		$("#lista").hide()
		$("#linha").css("text-align", "-webkit-right")
	})
	$("#listar").on("click", function(){
		$("#lista").show()
		$("#cadastro").hide()
		$("#linha").css("text-align", "-webkit-left")
	})
	
});

$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});


function validar()
{
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o nome", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}
	if($("#nu_telefone").val() == "")
	{
		$("#nu_telefone").focus();
		swal.fire("Erro", "Preencha o telefone", "error");
		$("#nu_telefone").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_telefone").removeClass("is-invalid");	
		$("#nu_telefone").addClass("is-valid");
	}

	return true;
}
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
                         
						<a href="appFuncionario/ver_funcionario.php?id_funcionario=`+full[0]+`&id_tipo=1"" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Visualizar Cadastro">
                          <i class="la la-edit"></i>

                        </a>
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-funcionario="`+full[0]+`" > 
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				}, 
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_funcionario = $(this).data("funcionario");
			swal.fire({
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Funcionário?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appFuncionario/excluir_funcionario.php'
				        , type: 'post'
				        , data: {id_funcionario : id_funcionario}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							redirectTo("appFuncionario/listar_funcionario.php");				
				        }
				    });
	                
	            }
	        });
			
		}); 
	

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1()
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});