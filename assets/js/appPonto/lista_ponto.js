$(document).ready(function() {
	$('#filtro').keyup(function () {
		var texto = $(this).val().toLowerCase().split(' ');
	
		$('#lista .texto-card').each(function () {
			for (let i = 0; i < texto.length; i++) {
				const element = texto[i];
				var linha = $(this).text().toLowerCase(); 
				if (linha.indexOf(element) === -1) {
					$(this).closest('.coluna-card').hide();
					return true;
				} else {
					$(this).closest('.coluna-card').show(); 
				}
			}
		 
		});
	});
	$("#salvarPonto").on("click", function(e){ 
		if(validarPonto())
		{ 	
			var form = $("#form_ponto").get(0); 
			$.ajax({
				
		        url: 'appPonto/gravar_ponto.php'
				, data: $("#form_ponto").serialize()
		        , type: 'post'
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
		        , success: function sucesso(html) {
					swal.fire({
		                position: 'top-right',
		                type: 'success',
		                title: html,
		                showConfirmButton: true
		            });
					$('#modal_cadastro').modal('hide');
					$(document).on('hidden.bs.modal','#modal_cadastro', function () {
						redirectTo("appPonto/listar_ponto.php");
					});
					
		        }
				
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
	$("#adicionar").on("click", function(){
		$("#cadastro").show()
		$("#lista").hide()
		$("#linha").css("text-align", "-webkit-right")
	})
	$("#listar").on("click", function(){
		$("#lista").show()
		$("#cadastro").hide()
		$("#linha").css("text-align", "-webkit-left")
	})
	
});


$("#nu_valor_ponto").inputmask({
	'alias': 'numeric',
	'groupSeparator': '.',
	'autoGroup': true,
	'prefix': 'R$ ', 
});

$("#ds_tamanho").inputmask({
	'mask' : '9,99 x 9,99',
});

function validarPonto()
{

	if($("#ds_localidade").val() == "")
	{
		$("#ds_localidade").focus();
		swal.fire("Erro", "Preencha a localidade", "error");
		$("#ds_localidade").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_localidade").removeClass("is-invalid");	
		$("#ds_localidade").addClass("is-valid");
	}
	if($("#ds_descricao").val() == "")
	{
		$("#ds_descricao").focus();
		swal.fire("Erro", "Preencha a descrição", "error");
		$("#ds_descricao").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_descricao").removeClass("is-invalid");	
		$("#ds_descricao").addClass("is-valid");
	}
	if($("#ds_sentido").val() == "")
	{
		$("#ds_sentido").focus();
		swal.fire("Erro", "Preencha o sentido", "error");
		$("#ds_sentido").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_sentido").removeClass("is-invalid");	
		$("#ds_sentido").addClass("is-valid");
	}

	if($("#ds_latitude").val() == "")
	{
		$("#ds_latitude").focus();
		swal.fire("Erro", "Selecione a localização", "error");
		$("#ds_latitude").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_latitude").removeClass("is-invalid");	
		$("#ds_latitude").addClass("is-valid");
	}

	if($("#ds_foto").val() == "")
	{
		$("#ds_foto").focus();
		swal.fire("Erro", "Adicione uma foto", "error"); 
		$("#ds_foto").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_foto").removeClass("is-invalid");	
		$("#ds_foto").addClass("is-valid");
	}


	if($("#id_tipo option:selected").val() == "")
	{
		$("#id_tipo").focus();
		swal.fire("Erro", "Escolha o tipo", "error");
		$("#id_tipo").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo").removeClass("is-invalid");	
		$("#id_tipo").addClass("is-valid");
	}
	if($("#ds_tamanho").val() == "")
	{
		$("#ds_tamanho").focus();
		swal.fire("Erro", "Preencha o tamanho", "error");
		$("#ds_tamanho").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_tamanho").removeClass("is-invalid");	
		$("#ds_tamanho").addClass("is-valid");
	}

	if($("#id_tipo_cobranca option:selected").val() == "")
	{
		$("#id_tipo_cobranca").focus();
		swal.fire("Erro", "Escolha o tipo de cobrança", "error");
		$("#id_tipo_cobranca").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo_cobranca").removeClass("is-invalid");	
		$("#id_tipo_cobranca").addClass("is-valid");
	}
	if($("#nu_valor_ponto").val() == "")
	{
		$("#nu_valor_ponto").focus();
		swal.fire("Erro", "Preencha o valor", "error");
		$("#nu_valor_ponto").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_valor_ponto").removeClass("is-invalid");	
		$("#nu_valor_ponto").addClass("is-valid");
	}



	return true;
}
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table_outdoor');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

		});

		table.on('click', '#excluir', function() {
			var id_ponto = $(this).data("ponto");
			swal.fire({
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Ponto?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appPonto/excluir_ponto.php'
				        , type: 'post'
				        , data: {id_ponto : id_ponto}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							redirectTo("appPonto/listar_ponto.php");				
				        }
				    });
	                
	            }
	        });
			
		}); 
	

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	var initTable2 = function() {
		var table = $('#table_front');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
                         <a href="appPonto/ver_ponto.php?id_ponto=`+full[0]+`&id_tipo=2"" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Visualizar Cadastro">
                          <i class="la la-edit"></i>
                        </a>
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-ponto="`+full[0]+`" > 
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				
				
			],
		});
	
 
		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	

		table.on('click', '#excluir', function() {
			var id_ponto = $(this).data("ponto");
			swal.fire({
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Ponto?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appPonto/excluir_ponto.php'
				        , type: 'post'
				        , data: {id_ponto : id_ponto}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							redirectTo("appPonto/listar_ponto.php");				
				        }
				    });
	                
	            }
	        });
			
		}); 
		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1(),
			initTable2();
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});