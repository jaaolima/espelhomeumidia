
$(document).ready(function() {

	$("#adicionar").on("click", function(e){ 
		if(validarAlugado())
		{
			id_ponto = $("#id_ponto_cadastro").val();
			id_tipo = $("#id_tipo").val();
			$.ajax({ 
				url: 'appPonto/gravar_alugado.php'
				, data: $("#form_bisemana").serialize()
				, type: 'post'
				, success: function(html) {
					swal.fire({
		                position: 'top-right',
		                type: 'success',
		                title: html,
		                showConfirmButton: true
		            });
					$('#modalBisemana').modal('hide');
					$(document).on('hidden.bs.modal','#modalBisemana', function () {
						redirectTo("appPonto/ver_ponto.php?id_ponto="+id_ponto+"&id_tipo="+id_tipo); 
					});
				}
				, error: function (data) { 
					swal.fire("Erro", data.responseText, "error");  
				}
			});		
			
		}
	});	

	$("#adicionar_documento").on("click", function(e){ 
		if(validarDocumento())
		{ 	
			id_ponto = $("#id_ponto_cadastro").val();
			id_tipo = $("#id_tipo").val();
			var form = $("#form_documento").get(0); 
			$.ajax({
				
		        url: 'appDocumento/gravar_documento.php'
				, data: $("#form_documento").serialize()
		        , type: 'post' 
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
		        , success: function sucesso(html) {
					swal.fire({
		                position: 'top-right',
		                type: 'success',
		                title: html,
		                showConfirmButton: true
		            });
					$('#modalDocumento').modal('hide');
					$(document).on('hidden.bs.modal','#modalDocumento', function () {
						redirectTo("appPonto/ver_ponto.php?id_ponto="+id_ponto+"&id_tipo="+id_tipo);
					});
					
		        }
				 
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		 
		}	
	});
	$("#alterar").on("click", function(){
		$("#cadastro").show()
		$("#detalhe").hide()
		$("#linha").css("text-align", "-webkit-right")
	})
	$("#detalhar").on("click", function(){
		$("#detalhe").show()
		$("#cadastro").hide()
		$("#linha").css("text-align", "-webkit-left") 
	})

	$("#id_tipo_aluguel").change(function(){
		if($(this).val() == 1){
			$("#bisemanal").show();
			$("#mensal").hide();
		}
		if($(this).val() == 2){
			$("#mensal").show();
			$("#bisemanal").hide(); 
		} 
	})

	$('.select2').select2();
	
	    
	$("#salvar").on("click", function(e){ 
		if(validarPonto())
		{ 
			id_ponto = $("#id_ponto_cadastro").val();
			id_tipo = $("#id_tipo").val();
			var form = $("#form_ponto").get(0); 
			$.ajax({
		        url: 'appPonto/gravar_alterar_ponto.php'
		        , type: 'post'
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
		        , success: function(html) {
		        	swal.fire({
		                position: 'top-right',
		                type: 'success',
		                title: html,
		                showConfirmButton: true
		            });
					$('#modal_editar').modal('hide');
					$(document).on('hidden.bs.modal','#modal_editar', function () {
						redirectTo("appPonto/ver_ponto.php?id_ponto="+id_ponto+"&id_tipo="+id_tipo) 
					});
					          
		        }
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
	
});


// $("#nu_localidade").inputmask({
// 	"mask": "-99.999999/-99.999999",
// 	autoUnmask: false,
// });

$("#nu_valor_ponto").inputmask({
	'alias': 'numeric',
	'groupSeparator': '.',
	'autoGroup': true,
	'prefix': 'R$ ', 
});

$("#nu_valor").inputmask({
	'alias': 'numeric',
	'groupSeparator': '.',
	'autoGroup': true,
	'prefix': 'R$ ',
});

$("#ds_tamanho").inputmask({
	'mask' : '9,99 x 9,99',
});

function fecharModal(){
	$('#modalBisemana').modal('hide');

	if(!$('#modalBisemana').hasClass("show")){
		console.log("não tem");
		return true;
	}

	$('#modalDocumento').modal('hide');

	if(!$('#modalDocumento').hasClass("show")){
		console.log("não tem");
		return true;
	}
	
}

function validarDocumento() 
{
	if($("#st_tipo_perfil option:selected").val() == "")
	{
		$("#st_tipo_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#st_tipo_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#st_tipo_perfil").removeClass("is-invalid");	
		$("#st_tipo_perfil").addClass("is-valid");
	}

	if($("#id_perfil option:selected").val() == "")
	{
		$("#id_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#id_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_perfil").removeClass("is-invalid");	
		$("#id_perfil").addClass("is-valid");
	}
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o nome do documento", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}

	return true;
}

function validarPonto()
{
	if($("#ds_localidade").val() == "")
	{
		$("#ds_localidade").focus();
		swal.fire("Erro", "Preencha a localidade", "error");
		$("#ds_localidade").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_localidade").removeClass("is-invalid");	
		$("#ds_localidade").addClass("is-valid");
	}
	if($("#ds_descricao").val() == "")
	{
		$("#ds_descricao").focus();
		swal.fire("Erro", "Preencha a descrição", "error");
		$("#ds_descricao").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_descricao").removeClass("is-invalid");	
		$("#ds_descricao").addClass("is-valid");
	}
	if($("#ds_sentido").val() == "")
	{
		$("#ds_sentido").focus();
		swal.fire("Erro", "Preencha o sentido", "error");
		$("#ds_sentido").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_sentido").removeClass("is-invalid");	
		$("#ds_sentido").addClass("is-valid");
	}

	if($("#ds_latitude").val() == "")
	{
		$("#ds_latitude").focus();
		swal.fire("Erro", "Selecione a localização", "error");
		$("#ds_latitude").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_latitude").removeClass("is-invalid");	
		$("#ds_latitude").addClass("is-valid");
	}


	if($("#id_tipo option:selected").val() == "")
	{
		$("#id_tipo").focus();
		swal.fire("Erro", "Escolha o tipo", "error");
		$("#id_tipo").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo").removeClass("is-invalid");	
		$("#id_tipo").addClass("is-valid");
	}
	if($("#ds_tamanho").val() == "")
	{
		$("#ds_tamanho").focus();
		swal.fire("Erro", "Preencha o tamanho", "error");
		$("#ds_tamanho").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_tamanho").removeClass("is-invalid");	
		$("#ds_tamanho").addClass("is-valid");
	}

	if($("#id_tipo_cobranca option:selected").val() == "")
	{
		$("#id_tipo_cobranca").focus();
		swal.fire("Erro", "Escolha o tipo de cobrança", "error");
		$("#id_tipo_cobranca").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo_cobranca").removeClass("is-invalid");	
		$("#id_tipo_cobranca").addClass("is-valid");
	}
	if($("#nu_valor_ponto").val() == "")
	{
		$("#nu_valor_ponto").focus();
		swal.fire("Erro", "Preencha o valor", "error");
		$("#nu_valor_ponto").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_valor_ponto").removeClass("is-invalid");	
		$("#nu_valor_ponto").addClass("is-valid");
	}



	return true;
}

function validarAlugado()
{
	if($("#id_cliente option:selected").val() == "")
	{
		$("#id_cliente").focus();
		swal.fire("Erro", "Selecione um cliente", "error");
		$("#id_cliente").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_cliente").removeClass("is-invalid");	
		$("#id_cliente").addClass("is-valid");
	}
	if($("#nu_valor").val() == "")
	{
		$("#nu_valor").focus();
		swal.fire("Erro", "Preencha o valor", "error");
		$("#nu_valor").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_valor").removeClass("is-invalid");	
		$("#nu_valor").addClass("is-valid");
	}

	if($("#id_tipo_aluguel option:selected").val() == "")
	{
		$("#id_tipo_aluguel").focus();
		swal.fire("Erro", "Selecione um tipo de aluguel", "error");
		$("#id_tipo_aluguel").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#id_tipo_aluguel").removeClass("is-invalid");	 
		$("#id_tipo_aluguel").addClass("is-valid");
	}

	if($("#id_tipo_aluguel option:selected").val() === "1"){

		if ($('input[name="bisemana[]"]:checked').length == 0)
		{
			$("input[name='bisemana[]']").focus();
			swal.fire("Aviso", "Selecione uma das opções", "warning");      
			return false;	
		}
	}

	if($("#id_tipo_aluguel option:selected").val() === "2"){

		if($("#dt_inicial").val() == "")
		{
			$("#dt_inicial").focus();
			swal.fire("Erro", "Preencha a Data inicial", "error");
			$("#dt_inicial").addClass("is-invalid");
			return false;	
		}
		else
		{
			$("#dt_inicial").removeClass("is-invalid");	
			$("#dt_inicial").addClass("is-valid");
		}

		if ($('input[name="meses"]:checked').length == 0)
		{
			$("input[name='meses']").focus();
			swal.fire("Aviso", "Selecione uma das opções", "warning");      
			return false;	
		}

	}
	


	return true;
}


var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table_bisemana'); 

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},


			columnDefs: [

				{
					targets: 0,
					visible: false 
				},
				
				
			],
		});
	

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	var initTable2 = function() {
		var table = $('#table_aluguel');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-ponto="`+full[0]+`" >
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_alugado = $(this).data("ponto");
			swal.fire({ 
	            title: 'Tem certeza?',
	            text: "Desejar excluir a locação?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appPonto/excluir_alugado.php'
				        , type: 'post'
				        , data: {id_alugado : id_alugado}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							var id_ponto = $("#id_ponto").val();
							var id_tipo = $("#id_tipo").val();
							redirectTo("appPonto/ver_ponto.php?id_ponto="+id_ponto+"&id_tipo="+id_tipo);				
				        }
				    });
	                
	            }
	        });
			
		}); 

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	var initTable3 = function() {
		var table = $('#table_documento');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 5,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-documento="`+full[0]+`" >
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				},
				{
					targets: 0,
					visible: false
				},
				
				
			],
		});

		table.on('click', '#excluir', function() {
			var id_documento = $(this).data("documento");
			swal.fire({ 
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Documento?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appDocumento/excluir_documento.php'
				        , type: 'post'
				        , data: {id_documento : id_documento}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							var id_ponto = $("#id_ponto").val();
							var id_tipo = $("#id_tipo").val();
							redirectTo("appPonto/ver_ponto.php?id_ponto="+id_ponto+"&id_tipo="+id_tipo);					
				        }
				    });
	                
	            }
	        });
			
		}); 

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
			initTable2();
			initTable3();
		},

	};

}();



jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});