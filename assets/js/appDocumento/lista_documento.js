$(document).ready(function() {
	    
	$("#salvar").on("click", function(e){ 
		if(validar())
		{ 	
			var form = $("#form_ponto").get(0); 
			$.ajax({
				
		        url: 'appDocumento/gravar_documento.php'
				, data: $("#form_ponto").serialize()
		        , type: 'post'
				, data: new FormData(form)
				, mimeType: 'multipart/form-data'  
				, processData: false
				, contentType: false
		        , success: function sucesso(html) {

					swal.fire({
						position: 'top-right',
						type: 'success',
						title: html,
						showConfirmButton: true
					});
					redirectTo("appDocumento/listar_documento.php");
					
		        }
				
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		 
		}	
	});

	$("#st_tipo_perfil").on("change", function() {
		var st_tipo_perfil = $("#st_tipo_perfil option:selected").val(); 
		if(st_tipo_perfil != 4){
			$.ajax({
				url: 'appUsuario/listar_options_perfil.php'
				, type: 'post'
				, data: {st_tipo_perfil : st_tipo_perfil}
				, success: function(html) {
					$("#id_perfil").empty();
					$("#id_perfil").append(html);     
				}
			}); 
		}
		if(st_tipo_perfil == 4){
			$("#perfil").hide();
		}else{
			$("#perfil").show();
		}
	}); 

	$("#adicionar").on("click", function(){
		$("#cadastro").show()
		$("#lista").hide()
		$("#linha").css("text-align", "-webkit-right")
	})
	$("#listar").on("click", function(){
		$("#lista").show()
		$("#cadastro").hide()
		$("#linha").css("text-align", "-webkit-left")
	})
	
});

$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});


function validar()
{
	if($("#st_tipo_perfil option:selected").val() == "")
	{
		$("#st_tipo_perfil").focus();
		swal.fire("Erro", "Selecione o perfil", "error");
		$("#st_tipo_perfil").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#st_tipo_perfil").removeClass("is-invalid");	
		$("#st_tipo_perfil").addClass("is-valid");
	}

	if($("#st_tipo_perfil option:selected").val() != "4"){
		if($("#id_perfil option:selected").val() == "")
		{
			$("#id_perfil").focus();
			swal.fire("Erro", "Selecione o perfil", "error");
			$("#id_perfil").addClass("is-invalid");
			return false;	
		}
		else
		{
			$("#id_perfil").removeClass("is-invalid");	
			$("#id_perfil").addClass("is-valid");
		}
	}
	
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		swal.fire("Erro", "Preencha o nome do documento", "error");
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}

	return true;
}
var DatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#table');

		// begin first table
		table.DataTable({
			responsive: true,
			retrieve: true, 
			

			//== DOM Layout settings
			dom: `f<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Mostrar _MENU_',
			},
			"oLanguage": {
			    "sSearch": "<span>Pesquisar:</span> _INPUT_",
			    "sLengthMenu": "<span>Mostrar:</span> _MENU_",
			    "sInfo": "<span>Mostrando </span>_START_ até _END_ de _TOTAL_",
			    "sZeroRecords": "Não existem dados cadastrados",
			    "sInfoEmpty": "<span>Mostrando </span>0 até 0 de 0",
			    "oPaginate": { "sFirst": "Primeira", "sLast": "Última", "sNext": ">", "sPrevious": "<" }
		    },

			//== Order settings
			order: [[1, 'asc']],


			columnDefs: [

				{ 
					targets: -1,
					title: 'Ações',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        
						<a id="excluir"class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Deletar" data-documento="`+full[0]+`" > 
                          <i class="la la-remove"></i>
                        </a>
                        `;
					},
				}, 
				{
					targets: 0,
					visible: false
				},
				
				
			],
		}); 

		table.on('click', '#excluir', function() {
			var id_documento = $(this).data("documento");
			swal.fire({
	            title: 'Tem certeza?',
	            text: "Desejar excluir o Documento?",
	            type: 'warning',
	            showCancelButton: true,
	            cancelButtonColor: '#fd397a',
	            confirmButtonText: 'Sim, posseguir!',
				cancelButtonText: 'Cancelar'
	        }).then(function(result) {
	            if (result.value) {
					$.ajax({
				        url: 'appDocumento/excluir_documento.php'
				        , type: 'post'
				        , data: {id_documento : id_documento}
				        , success: function(html) {
							swal.fire('Pronto!',html,'success');
							redirectTo("appDocumento/listar_documento.php");				
				        }
				    });
	                
	            }
	        });
			
		}); 
	

		table.on('change', 'tbody tr .m-checkbox', function() {
			$(this).parents('tr').toggleClass('active');
		});	
		
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1()
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});