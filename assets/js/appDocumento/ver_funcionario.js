$(document).ready(function() {
	    
	id_funcionario = $("#id_funcionario").val();
	
	$("#salvar").on("click", function(e){ 
		if(validar())
		{ 
			$.ajax({ 
		        url: 'appFuncionario/gravar_alterar_funcionario.php'
				, data: $("#form_cliente").serialize()
		        , type: 'post'
		        , success: function(html) {
		        	swal.fire({
		                position: 'top-right',
		                type: 'success', 
		                title: html,
		                showConfirmButton: true
		            });	
					if(fecharModal()){ 
						redirectTo("appFuncionario/ver_funcionario.php?id_funcionario="+id_funcionario); 
					}	
					  
		        }
				, error: function (data) {
					swal.fire("Erro", data.responseText, "error");
				}
		    });		
		}	
	});
}); 

$("#nu_telefone").inputmask({
	"mask": "(99)99999-9999",
	autoUnmask: false,
});

function fecharModal(){
	$("#modal").modal('hide');
	return true;
}

function validar()
{
	if($("#ds_nome").val() == "")
	{
		$("#ds_nome").focus();
		/*swal.fire("Erro", "Preencha a descrição", "error");*/
		$("#ds_nome").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#ds_nome").removeClass("is-invalid");	
		$("#ds_nome").addClass("is-valid");
	}
	if($("#nu_telefone").val() == "")
	{
		$("#nu_telefone").focus();
		//swal.fire("Erro", "Preencha a descrição", "error");
		$("#nu_telefone").addClass("is-invalid");
		return false;	
	}
	else
	{
		$("#nu_telefone").removeClass("is-invalid");	
		$("#nu_telefone").addClass("is-valid");
	}

	return true;
}

jQuery(document).ready(function() {
	DatatablesBasicBasic.init();
});