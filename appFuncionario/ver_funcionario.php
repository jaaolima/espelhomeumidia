<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);

    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }

	require_once("../Classes/Funcionario.php");
	require_once("../Classes/Ponto.php");
    require_once("../Classes/Documento.php");

    $id_funcionario = $_REQUEST["id_funcionario"];
    $id_usuario = $_SESSION["id_usuario"];

    $funcionario = new Funcionario(); 
    $documento = new Documento();

    $dados = $funcionario->BuscarDadosFuncionario($id_funcionario);
    $listaDocumentos = $documento->listarDocumentoFuncionario($id_funcionario);
?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="my-8 text-center">
        <h1 class="mt-5 font-weight-bolder"><?php echo $dados["ds_nome"] ?></h1>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <h3 class="font-weight-bolder">Detalhes:</h3> 
                <div class="row">
                    <div class="d-block mt-2 col-6">
                        <div class="d-flex">
                            <p>Nome: </p>
                            <span class="ml-2 font-weight-bolder"><?php echo $dados["ds_nome"] ?></span>
                        </div>
                        <div class="d-flex">
                            <p>Telefone: </p>
                            <span class=" ml-2 font-weight-bolder"><?php  echo $dados["nu_telefone"] ?></span>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal">Editar</button>
                </div>
                <div class="modal fade" id="modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Editar Funcionario: <?php echo $dados["ds_nome"] ?></h5>
                            </div>
                            <div class="modal-body">
                                 <form id="form_cliente">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Nome: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="ds_nome" name="ds_nome" value="<?php echo $dados['ds_nome']?>"/>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Telefone: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="nu_telefone" name="nu_telefone" value="<?php echo $dados['nu_telefone']?>"/>
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control" id="id_funcionario" name="id_funcionario" value="<?php echo $dados['id_funcionario']?>"/>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <button type="button" id="salvar" class="btn btn-primary">Salvar</button>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card card-custom w-100 bgi-no-repeat bgi-size-cover gutter-b bg-white p-8" >
                <h3 class="font-weight-bolder">Documentos:</h3>
                <table class="table table-responsive d-xl-table" id="table_documento">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Documento</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while ($dadosDocs = $listaDocumentos->fetch())
                            {
                                echo "<tr>
                                        <td>".$dadosDocs['id_documento']."</td>
                                        <td><a target='_blank' href='".$dadosDocs['ds_documento']."'>".$dadosDocs['ds_nome']."</a></td>
                                        <td nowrap></td>
                                    </tr>";
                                
                            }
                        ?>
                    </tbody>
                </table>
                <div class="text-right mt-2">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalDocumento">Adicionar Documento</button>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalDocumento" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Adicionar Documento</h5>
                    </div>
                    <div class="modal-body">
                        <form id="form_documento">
                            <div class="row">
                                <input type="hidden" name="st_tipo_perfil" id="st_tipo_perfil" value="2">
                                <input type="hidden" name="id_perfil" id="id_perfil" value="<?php echo $id_funcionario; ?>">
                                <div class="col-6">
                                    <label>Documento: <span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" id="ds_documento" name="ds_documento" />
                                </div>
                                <div class="col-6">
                                    <label>Nome do documento: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                                </div>
                            </div>
                            <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" id="adicionar_documento" class="btn btn-primary">Adicionar</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appFuncionario/ver_funcionario.js" type="text/javascript"></script>
</body>

</html>