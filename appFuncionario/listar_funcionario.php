<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	require_once("../Classes/Funcionario.php");

    $funcionario = new Funcionario(); 

    $id_usuario	    = $_SESSION['id_usuario'];
    $retorno = $funcionario->listarFuncionario($id_usuario);
?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="my-8 text-center">
        <h1 class="text-dark font-weight-bolder">Funcionários</h1>
    </div>
    <div class="row">
        <div class="col-6 text-center ">
            <h3 id="listar" class="linha-botao">lista</h3> 
        </div>
        <div class="col-6 text-center">
            <h3 id="adicionar" class="linha-botao">cadastro</h3>
        </div>
    </div>
    <div class="text-end" id="linha" >
        <div style="height:10px; width:50%;" class="bg-primary " >
        </div>
    </div>

    
    <div class="row" id="lista">

        <div class="col-12">
            <div class="card card-custom bg-white"  >
                <div class="card-body">
                    <table class="table table-hover table-responsive d-xl-table" id="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Funcionário</th>
                                <th>Telefone</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                while ($dados = $retorno->fetch())
                                {
                                    
                                    echo "<tr>
                                            <td>".$dados['id_funcionario']."</td>
                                            <td>".$dados['ds_nome']."</td>
                                            <td>".$dados['nu_telefone']."</td>
                                            <td nowrap></td>
                                        </tr>";
                                }
                            ?>
                            
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    
    </div> 
    
    <div class="w-100" id="cadastro" style="display:none;">
        <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white"  >
            <div class="card-header">
                <div class="card-title">
                    <h3>Adicione:</h3>
                </div>
            </div>
            <div class="card-body">
                <form id="form_ponto">
                    <div class="row">
                        <div class="col-6">
                            <label>Nome: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                        </div>
                        <div class="col-6">
                            <label>Telefone: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nu_telefone" name="nu_telefone" />
                        </div>
                    </div>
                    <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" id="salvar" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
       
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appFuncionario/lista_funcionario.js" type="text/javascript"></script>
</body>

</html>