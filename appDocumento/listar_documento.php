<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
    session_start();
    if($_SERVER['REQUEST_URI'] == "http://www.espelhomeumidia.com.br/principal.php"){
        session_save_path("/tmp");
    }
	require_once("../Classes/Documento.php");

    $Documento = new Documento(); 

    $id_usuario	    = $_SESSION['id_usuario'];
    $retorno = $Documento->listarDocumento($id_usuario);
?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="my-8 text-center">
        <h1 class="text-dark font-weight-bolder">Documentos</h1>
    </div>
    <div class="row">
        <div class="col-6 text-center ">
            <h3 id="listar" class="linha-botao">lista</h3> 
        </div>
        <div class="col-6 text-center">
            <h3 id="adicionar" class="linha-botao">cadastro</h3>
        </div>
    </div>
    <div class="text-end" id="linha" >
        <div style="height:10px; width:50%;" class="bg-primary " >
        </div>
    </div>

    
    <div class="row" id="lista">

        <div class="col-12">
            <div class="card card-custom bg-white"  >
                <div class="card-body">
                    <table class="table table-hover table-responsive d-xl-table" id="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Perfil</th>
                                <th>Documento</th>
                                <th>Ações</th>
                            </tr>
                        </thead> 
                        <tbody> 
                            <?php
                                while ($dados = $retorno->fetch())
                                {
                                    if($dados["st_tipo_perfil"] == 1){
                                        echo "<tr>
                                            <td>".$dados['id_documento']."</td>
                                            <td>".$dados['ds_cliente']."</td>
                                            <td><a target='_blank' href='".$dados['ds_documento']."'>".$dados['ds_nome']."</a></td>
                                            <td nowrap></td>
                                        </tr>";
                                    }
                                    if($dados["st_tipo_perfil"] == 2){
                                        echo "<tr>
                                            <td>".$dados['id_documento']."</td>
                                            <td>".$dados['ds_funcionario']."</td>
                                            <td><a  target='_blank' href='".$dados['ds_documento']."'>".$dados['ds_nome']."</a></td>
                                            <td nowrap></td>
                                        </tr>";
                                    }
                                    if($dados["st_tipo_perfil"] == 3){
                                        echo "<tr>
                                            <td>".$dados['id_documento']."</td>
                                            <td>".$dados['ds_localidade']."</td>
                                            <td><a target='_blank' href='".$dados['ds_documento']."'>".$dados['ds_nome']."</a></td>
                                            <td nowrap></td>
                                        </tr>";
                                    }
                                    if($dados["st_tipo_perfil"] == 4){
                                        echo "<tr>
                                            <td>".$dados['id_documento']."</td>
                                            <td>Empresa</td>
                                            <td><a target='_blank' href='".$dados['ds_documento']."'>".$dados['ds_nome']."</a></td>
                                            <td nowrap></td>
                                        </tr>";
                                    }
                                    
                                }
                            ?>
                            
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    
    </div> 
    
    <div class="w-100" id="cadastro" style="display:none;">
        <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-white"  >
            <div class="card-header">
                <div class="card-title">
                    <h3>Adicione:</h3> 
                </div>
            </div>
            <div class="card-body">
                <form id="form_ponto">
                    <div class="row">
                        <div class="col-3">
                            <label>Tipo perfil: <span class="text-danger">*</span></label>
                            <select class="form-control" name="st_tipo_perfil" id="st_tipo_perfil">
                                <option  value="">Selecione...</option>
                                <option  value="1" >Cliente</option>
                                <option  value="2">Funcionário</option> 
                                <option  value="3">Ponto</option> 
                                <option  value="4">Empresa</option> 
                            </select>
                        </div>
                        <div class="col-6" id="perfil">
                            <label>Perfil: <span class="text-danger">*</span></label>
                            <select class="form-control" name="id_perfil" id="id_perfil">
                                <option  value="">Selecione...</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label>Documento: <span class="text-danger">*</span></label>
                            <input type="file" class="form-control" id="ds_documento" name="ds_documento" />
                        </div>
                        <div class="col-6">
                            <label>Nome do documento: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ds_nome" name="ds_nome" />
                        </div>
                    </div>
                    <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario; ?>">
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" id="salvar" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
       
    <script src="./assets/js/datatables.bundle.js" type="text/javascript"></script>
    <script src="./assets/js/appDocumento/lista_documento.js" type="text/javascript"></script>
</body>

</html>