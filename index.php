
<?php

?>
<!DOCTYPE html> 
<html lang="en"> 
	<!--begin::Head-->
    <head><base href="../../../../"> 
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Espelho Meu | Login</title>
        <link rel="canonical" href="https://keenthemes.com/metronic" />
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Page Vendors Styles(used by this page)-->
        <link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles-->
        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />	
        <!--end::Global Theme Styles-->
        <!--begin::Layout Themes(used by all pages)-->
        <!--end::Layout Themes-->
        <link rel="shortcut icon" href="assets/media/logoPp.png" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed subheader-enabled page-loading"  style="overflow-x:hidden;">
		<div class="w-100">
			<div class="w-100 m-0 h-auto p-0 d-flex justify-content-between align-items-center container-fluid"  style="background-color: #000000 !important;position:fixed; z-index:98;"> 
				<div>
					<h2 class="ml-3 text-white" >
						Painel Pro
					</h2>
				</div>
				<div class="my-2 mr-2 d-flex">    
					<div style="margin-top:17px;">
						<a href='#cadastro_login' class="mt-3 mr-3 rounded" style="padding: 8px; background-color: transparent; border: 1px #ffffff solid; color: #ffffff;">
							Criar Login
						</a>
						<a href='#cadastro_login' class="mt-3 mr-3 rounded" style="padding: 8px; background-color: white; color: #000000;">
							Entrar
						</a>
					</div>
					<img src="assets/media/logoPp.png" class="max-h-55px" alt="" />
				</div>
			</div>
			<!--begin::Main-->
			<div class="d-flex flex-column flex-root "> 
				<!--begin::Login-->
				<div class="row" style="background-image: url(assets/media/fundo-outdoor.avif) ;background-position: center top;background-size: 100% auto;">
					<div style='backdrop-filter: blur(8px);' class="text-center w-100">
						<div class="mt-4" style="text-align:center; color:white;">
							<h1>Painel Pro</h1>
						</div>
						<p class="p-10 m-10 text-white" style="font-size:16px;">
							Bem-vindo à <a class="text-primary" href="painelpro.com">Painel Pro</a>, oferecemos uma plataforma abrangente que simplifica o processo de planejamento, execução e análise de campanhas de mídia exterior.
						</p>
						<div class="w-100 text-center">
							<h3 class="text-white mb-10">Nossos Serviços</h3>
						</div>
						<div class="row m-10" style="justify-content:space-evenly;">
							<div class="col-xxl-3 col-sm-12 col-xl-5 card card-inicial mt-2">
								<div class="justify-content-center mt-10"> 
									<svg xmlns="http://www.w3.org/2000/svg" class="text-primary" width="100" height="100" fill="currentColor" class="bi bi-bar-chart-line" viewBox="0 0 16 16">
										<path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z"/>
									</svg>
								</div>
								<h3 class="text-center my-6" style="font-weight:900;">Monitoramento em Tempo Real</h3>
								<!-- <p class="px-10" style="font-size:12px;text-align:center;">
									Acompanhe a evolução de suas campanhas com nossa plataforma de monitoramento em tempo real. Saiba onde suas mídias estão localizadas, e obtenha insights em tempo real para otimizar o impacto da sua mensagem.
								</p> -->
							</div>
							<div class="col-xxl-3 col-sm-12 col-xl-5 card card-inicial mt-2">
								<div class="justify-content-center mt-10">
									<svg xmlns="http://www.w3.org/2000/svg"  class="text-primary" width="100" height="100" fill="currentColor" class="bi bi-filetype-docx" viewBox="0 0 16 16">
										<path fill-rule="evenodd" d="M14 4.5V11h-1V4.5h-2A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v9H2V2a2 2 0 0 1 2-2h5.5L14 4.5Zm-6.839 9.688v-.522a1.54 1.54 0 0 0-.117-.641.861.861 0 0 0-.322-.387.862.862 0 0 0-.469-.129.868.868 0 0 0-.471.13.868.868 0 0 0-.32.386 1.54 1.54 0 0 0-.117.641v.522c0 .256.04.47.117.641a.868.868 0 0 0 .32.387.883.883 0 0 0 .471.126.877.877 0 0 0 .469-.126.861.861 0 0 0 .322-.386 1.55 1.55 0 0 0 .117-.642Zm.803-.516v.513c0 .375-.068.7-.205.973a1.47 1.47 0 0 1-.589.627c-.254.144-.56.216-.917.216a1.86 1.86 0 0 1-.92-.216 1.463 1.463 0 0 1-.589-.627 2.151 2.151 0 0 1-.205-.973v-.513c0-.379.069-.704.205-.975.137-.274.333-.483.59-.627.257-.147.564-.22.92-.22.357 0 .662.073.916.22.256.146.452.356.59.63.136.271.204.595.204.972ZM1 15.925v-3.999h1.459c.406 0 .741.078 1.005.235.264.156.46.382.589.68.13.296.196.655.196 1.074 0 .422-.065.784-.196 1.084-.131.301-.33.53-.595.689-.264.158-.597.237-.999.237H1Zm1.354-3.354H1.79v2.707h.563c.185 0 .346-.028.483-.082a.8.8 0 0 0 .334-.252c.088-.114.153-.254.196-.422a2.3 2.3 0 0 0 .068-.592c0-.3-.04-.552-.118-.753a.89.89 0 0 0-.354-.454c-.158-.102-.361-.152-.61-.152Zm6.756 1.116c0-.248.034-.46.103-.633a.868.868 0 0 1 .301-.398.814.814 0 0 1 .475-.138c.15 0 .283.032.398.097a.7.7 0 0 1 .273.26.85.85 0 0 1 .12.381h.765v-.073a1.33 1.33 0 0 0-.466-.964 1.44 1.44 0 0 0-.49-.272 1.836 1.836 0 0 0-.606-.097c-.355 0-.66.074-.911.223-.25.148-.44.359-.571.633-.131.273-.197.6-.197.978v.498c0 .379.065.704.194.976.13.271.321.48.571.627.25.144.555.216.914.216.293 0 .555-.054.785-.164.23-.11.414-.26.551-.454a1.27 1.27 0 0 0 .226-.674v-.076h-.765a.8.8 0 0 1-.117.364.699.699 0 0 1-.273.248.874.874 0 0 1-.401.088.845.845 0 0 1-.478-.131.834.834 0 0 1-.298-.393 1.7 1.7 0 0 1-.103-.627v-.495Zm5.092-1.76h.894l-1.275 2.006 1.254 1.992h-.908l-.85-1.415h-.035l-.852 1.415h-.862l1.24-2.015-1.228-1.984h.932l.832 1.439h.035l.823-1.439Z"/>
									</svg>
								</div>
								<h3 class="text-center my-6 " style="font-weight:900;">Gestão Documental Simplificada</h3>
								<!-- <p class="px-10" style="font-size:12px;text-align:center;">
									Armazene e gerencie documentos essenciais de campanha em um só lugar. Faça o upload de designs, contratos e outros documentos importantes, mantendo todos os detalhes organizados e acessíveis.
								</p> -->
							</div>
							<div class="col-xxl-3 col-sm-12 col-xl-5 card card-inicial mt-2">
								<div class="justify-content-center mt-10">
									<svg xmlns="http://www.w3.org/2000/svg" class="text-primary" width="100" height="100" fill="currentColor" class="bi bi-file-earmark-slides" viewBox="0 0 16 16">
										<path d="M5 6a.5.5 0 0 0-.496.438l-.5 4A.5.5 0 0 0 4.5 11h3v2.016c-.863.055-1.5.251-1.5.484 0 .276.895.5 2 .5s2-.224 2-.5c0-.233-.637-.429-1.5-.484V11h3a.5.5 0 0 0 .496-.562l-.5-4A.5.5 0 0 0 11 6H5zm2  1.279a.125.125  .214l-2.13 1.28A.125.125"/>
										<path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z"/>
									</svg>
								</div>
								<h3 class="text-center my-6" style="font-weight:900;">Relatórios Detalhados</h3>
								<!-- <p class="px-10" style="font-size:12px;text-align:center;">
									Através da nossa plataforma, tenha acesso a relatórios detalhados que fornecem uma análise profunda do desempenho das suas campanhas. Visualize métricas-chave, como impressões, engajamento e ROI, para tomar decisões informadas.
								</p> -->
							</div>
						</div>	
						<p class="p-10 m-10 text-white" style="font-size:16px;">
							Junte-se a nós na jornada de elevar sua presença de marca ao próximo nível. Entre em contato conosco hoje mesmo e descubra como podemos ampliar o impacto da sua mensagem nas mídias exteriores.
						</p>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12 justify-content-center d-flex">
                    	<div class="card card-custom bg-white p-10" style="margin:10%;max-width: 500px;">

							<div class="login login-4 login-signin-on d-flex flex-row-fluid mt-10" id="kt_login">
								<div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat">
									<div class="login-form text-center p-7 position-relative overflow-hidden">
										<!--begin::Login Sign in form-->
										<div class="login-signin"  id="cadastro_login">
											<div class="mb-10">
												<h3>Login</h3>
											</div>
											<form class="form" id="kt_login_signin_form">
												<div class="form-group mb-5">
													<input class="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="Usuário" name="ds_usuario" autocomplete="off" id="ds_usuario" style="background-color: #007eff17;" />
												</div>
												<div class="form-group mb-5">
													<input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="Senha" name="ds_senha" id="ds_senha"  style="background-color: #007eff17;" />
												</div>
												<div class="form-group d-flex flex-wrap flex-center mt-10">
													<button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" id="entrar">Entrar</button>
												</div>
											</form>
										</div>
										<!--end::Login Sign in form-->
									</div>
								</div>
							</div>
						</div>		
					</div>

				</div>
				<!--end::Login-->
			</div>
		</div>
		<footer class="k text-center text-lg-start" style="background-color: black;">
			<!-- Copyright -->
			<div class="text-center p-3" style="color: white">
				© 2023 Copyright:
				<a href="https://painelpro.com/">Painelpro.com</a>
			</div>
			<!-- Copyright -->
		</footer>
		
		<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
        <!--end::Global Config-->
        <!--begin::Global Theme Bundle(used by all pages)-->
        <script src="assets/plugins/global/plugins.bundle.js"></script>
        <script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
        <script src="assets/js/scripts.bundle.js"></script>
        <script src="assets/js/scripts.bundle2.min.js"></script>
        <!--begin::Page Vendors(used by this page)-->
        <script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
        <!--end::Page Vendors-->
        <!--begin::Page Scripts(used by this page)-->
        <script src="assets/js/pages/crud/datatables/basic/basic.js"></script>
        <!--end::Page Scripts-->
        <!--end::Global Theme Bundle-->
        <!--begin::Page Vendors(used by this page)-->
        <script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
        <!--end::Page Vendors-->
        <!--begin::Page Scripts(used by this page)-->
		<script src="assets/js/appLogin/login.js"></script>
		<script src="assets/js/appLogin/valida_login.js"></script>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>